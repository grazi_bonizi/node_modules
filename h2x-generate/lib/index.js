'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _h2xTraverse = require('h2x-traverse');

var _h2xTraverse2 = _interopRequireDefault(_h2xTraverse);

var _Generator = require('./Generator');

var _Generator2 = _interopRequireDefault(_Generator);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function generate(ast, opts) {
  const generator = new _Generator2.default();
  (0, _h2xTraverse2.default)(ast, opts, generator);
  return generator.output;
}

exports.default = generate;