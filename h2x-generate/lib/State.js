'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});
class State {
  constructor() {
    this.output = '';
    this.level = 0;
  }

  indent() {
    this.level += 1;
  }

  deindent() {
    this.level -= 1;
  }

  writeLine(code) {
    this.output += `${'  '.repeat(this.level)}${code}\n`;
  }
}

exports.default = State;