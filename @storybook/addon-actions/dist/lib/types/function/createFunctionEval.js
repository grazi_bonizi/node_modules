'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = createFunctionEval;

var _isReserved = require('./isReserved');

var _isReserved2 = _interopRequireDefault(_isReserved);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function createFunctionEval(name) {
  var fnName = (0, _isReserved2.default)(name) ? name + '$' : name;

  // eslint-disable-next-line no-new-func
  return new Function('return function ' + fnName + '() {}')();
}