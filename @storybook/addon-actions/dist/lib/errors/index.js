'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.DecycleError = undefined;

var _DecycleError2 = require('./DecycleError');

var _DecycleError3 = _interopRequireDefault(_DecycleError2);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

exports.DecycleError = _DecycleError3.default;