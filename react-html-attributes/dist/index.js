'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});
var reactHtmlAttributes = require('./react-html-attributes.json');

exports.default = reactHtmlAttributes;

module.exports = reactHtmlAttributes; // for CommonJS compatibility