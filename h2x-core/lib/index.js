'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.generate = exports.traverse = undefined;
exports.transform = transform;

var _h2xParse = require('h2x-parse');

var _h2xParse2 = _interopRequireDefault(_h2xParse);

var _h2xGenerate = require('h2x-generate');

var _h2xGenerate2 = _interopRequireDefault(_h2xGenerate);

var _h2xTraverse = require('h2x-traverse');

var _h2xTraverse2 = _interopRequireDefault(_h2xTraverse);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

const reduceVisitors = (visitors, opts) => {
  Object.keys(visitors).forEach(key => {
    const visitor = visitors[key];
    if (typeof visitor === 'function') {
      opts[key] = opts[key] || [];
      opts[key].push(visitor);
    } else {
      opts[key] = opts[key] || {};
      reduceVisitors(visitor, opts[key]);
    }
    return opts;
  });
}; /* eslint-disable no-restricted-syntax */


const mergePlugins = plugins => plugins.reduce((opts, plugin) => {
  var _plugin = plugin(),
      _plugin$visitor = _plugin.visitor;

  const visitor = _plugin$visitor === undefined ? {} : _plugin$visitor;
  var _plugin$generator = _plugin.generator;
  const generator = _plugin$generator === undefined ? {} : _plugin$generator;

  reduceVisitors(visitor, opts.visitor);
  reduceVisitors(generator, opts.generator);
  return opts;
}, { visitor: {}, generator: {} });

exports.traverse = _h2xTraverse2.default;
exports.generate = _h2xGenerate2.default;
function transform(code, { plugins = [], state = {} } = {}) {
  const ast = (0, _h2xParse2.default)(code);

  var _mergePlugins = mergePlugins(plugins);

  const visitor = _mergePlugins.visitor,
        generator = _mergePlugins.generator;

  (0, _h2xTraverse2.default)(ast, visitor, state);
  return (0, _h2xGenerate2.default)(ast, generator);
}