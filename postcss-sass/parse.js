'use strict';

exports.__esModule = true;
exports.default = sassParse;

var _input = require('postcss/lib/input');

var _input2 = _interopRequireDefault(_input);

var _parser = require('./parser');

var _parser2 = _interopRequireDefault(_parser);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function sassParse(sass, opts) {
    var input = new _input2.default(sass, opts);

    var parser = new _parser2.default(input);
    parser.parse();

    return parser.root;
}
module.exports = exports['default'];
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInBhcnNlLmVzNiJdLCJuYW1lcyI6WyJzYXNzUGFyc2UiLCJzYXNzIiwib3B0cyIsImlucHV0IiwicGFyc2VyIiwicGFyc2UiLCJyb290Il0sIm1hcHBpbmdzIjoiOzs7a0JBSXdCQSxTOztBQUp4Qjs7OztBQUVBOzs7Ozs7QUFFZSxTQUFTQSxTQUFULENBQW1CQyxJQUFuQixFQUF5QkMsSUFBekIsRUFBK0I7QUFDMUMsUUFBTUMsUUFBUSxvQkFBVUYsSUFBVixFQUFnQkMsSUFBaEIsQ0FBZDs7QUFFQSxRQUFNRSxTQUFTLHFCQUFXRCxLQUFYLENBQWY7QUFDQUMsV0FBT0MsS0FBUDs7QUFFQSxXQUFPRCxPQUFPRSxJQUFkO0FBQ0giLCJmaWxlIjoicGFyc2UuanMiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgSW5wdXQgZnJvbSAncG9zdGNzcy9saWIvaW5wdXQnO1xuXG5pbXBvcnQgUGFyc2VyIGZyb20gJy4vcGFyc2VyJztcblxuZXhwb3J0IGRlZmF1bHQgZnVuY3Rpb24gc2Fzc1BhcnNlKHNhc3MsIG9wdHMpIHtcbiAgICBjb25zdCBpbnB1dCA9IG5ldyBJbnB1dChzYXNzLCBvcHRzKTtcblxuICAgIGNvbnN0IHBhcnNlciA9IG5ldyBQYXJzZXIoaW5wdXQpO1xuICAgIHBhcnNlci5wYXJzZSgpO1xuXG4gICAgcmV0dXJuIHBhcnNlci5yb290O1xufVxuIl19