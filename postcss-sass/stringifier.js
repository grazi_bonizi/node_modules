'use strict';

exports.__esModule = true;

var _stringifier = require('postcss/lib/stringifier');

var _stringifier2 = _interopRequireDefault(_stringifier);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

var SassStringifier = function (_Stringifier) {
    _inherits(SassStringifier, _Stringifier);

    function SassStringifier() {
        _classCallCheck(this, SassStringifier);

        return _possibleConstructorReturn(this, _Stringifier.apply(this, arguments));
    }

    SassStringifier.prototype.block = function block(node, start) {
        this.builder(start, node, 'start');
        if (node.nodes && node.nodes.length) {
            this.body(node);
        }
    };

    SassStringifier.prototype.decl = function decl(node) {
        _Stringifier.prototype.decl.call(this, node, false);
    };

    SassStringifier.prototype.comment = function comment(node) {
        var left = this.raw(node, 'left', 'commentLeft');
        var right = this.raw(node, 'right', 'commentRight');

        if (node.raws.inline) {
            this.builder('//' + left + node.text + right, node);
        } else {
            this.builder('/*' + left + node.text + right + '*/', node);
        }
    };

    return SassStringifier;
}(_stringifier2.default);

exports.default = SassStringifier;
module.exports = exports['default'];
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInN0cmluZ2lmaWVyLmVzNiJdLCJuYW1lcyI6WyJTYXNzU3RyaW5naWZpZXIiLCJibG9jayIsIm5vZGUiLCJzdGFydCIsImJ1aWxkZXIiLCJub2RlcyIsImxlbmd0aCIsImJvZHkiLCJkZWNsIiwiY29tbWVudCIsImxlZnQiLCJyYXciLCJyaWdodCIsInJhd3MiLCJpbmxpbmUiLCJ0ZXh0Il0sIm1hcHBpbmdzIjoiOzs7O0FBQUE7Ozs7Ozs7Ozs7OztJQUVNQSxlOzs7Ozs7Ozs7OEJBRUZDLEssa0JBQU1DLEksRUFBTUMsSyxFQUFPO0FBQ2YsYUFBS0MsT0FBTCxDQUFhRCxLQUFiLEVBQW9CRCxJQUFwQixFQUEwQixPQUExQjtBQUNBLFlBQUtBLEtBQUtHLEtBQUwsSUFBY0gsS0FBS0csS0FBTCxDQUFXQyxNQUE5QixFQUF1QztBQUNuQyxpQkFBS0MsSUFBTCxDQUFVTCxJQUFWO0FBQ0g7QUFDSixLOzs4QkFFRE0sSSxpQkFBS04sSSxFQUFNO0FBQ1AsK0JBQU1NLElBQU4sWUFBV04sSUFBWCxFQUFpQixLQUFqQjtBQUNILEs7OzhCQUVETyxPLG9CQUFRUCxJLEVBQU07QUFDVixZQUFNUSxPQUFRLEtBQUtDLEdBQUwsQ0FBU1QsSUFBVCxFQUFlLE1BQWYsRUFBd0IsYUFBeEIsQ0FBZDtBQUNBLFlBQU1VLFFBQVEsS0FBS0QsR0FBTCxDQUFTVCxJQUFULEVBQWUsT0FBZixFQUF3QixjQUF4QixDQUFkOztBQUVBLFlBQUtBLEtBQUtXLElBQUwsQ0FBVUMsTUFBZixFQUF3QjtBQUNwQixpQkFBS1YsT0FBTCxDQUFhLE9BQU9NLElBQVAsR0FBY1IsS0FBS2EsSUFBbkIsR0FBMEJILEtBQXZDLEVBQThDVixJQUE5QztBQUNILFNBRkQsTUFFTztBQUNILGlCQUFLRSxPQUFMLENBQWEsT0FBT00sSUFBUCxHQUFjUixLQUFLYSxJQUFuQixHQUEwQkgsS0FBMUIsR0FBa0MsSUFBL0MsRUFBcURWLElBQXJEO0FBQ0g7QUFDSixLOzs7OztrQkFJVUYsZSIsImZpbGUiOiJzdHJpbmdpZmllci5qcyIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCBTdHJpbmdpZmllciBmcm9tICdwb3N0Y3NzL2xpYi9zdHJpbmdpZmllcic7XG5cbmNsYXNzIFNhc3NTdHJpbmdpZmllciBleHRlbmRzIFN0cmluZ2lmaWVyIHtcblxuICAgIGJsb2NrKG5vZGUsIHN0YXJ0KSB7XG4gICAgICAgIHRoaXMuYnVpbGRlcihzdGFydCwgbm9kZSwgJ3N0YXJ0Jyk7XG4gICAgICAgIGlmICggbm9kZS5ub2RlcyAmJiBub2RlLm5vZGVzLmxlbmd0aCApIHtcbiAgICAgICAgICAgIHRoaXMuYm9keShub2RlKTtcbiAgICAgICAgfVxuICAgIH1cblxuICAgIGRlY2wobm9kZSkge1xuICAgICAgICBzdXBlci5kZWNsKG5vZGUsIGZhbHNlKTtcbiAgICB9XG5cbiAgICBjb21tZW50KG5vZGUpIHtcbiAgICAgICAgY29uc3QgbGVmdCAgPSB0aGlzLnJhdyhub2RlLCAnbGVmdCcsICAnY29tbWVudExlZnQnKTtcbiAgICAgICAgY29uc3QgcmlnaHQgPSB0aGlzLnJhdyhub2RlLCAncmlnaHQnLCAnY29tbWVudFJpZ2h0Jyk7XG5cbiAgICAgICAgaWYgKCBub2RlLnJhd3MuaW5saW5lICkge1xuICAgICAgICAgICAgdGhpcy5idWlsZGVyKCcvLycgKyBsZWZ0ICsgbm9kZS50ZXh0ICsgcmlnaHQsIG5vZGUpO1xuICAgICAgICB9IGVsc2Uge1xuICAgICAgICAgICAgdGhpcy5idWlsZGVyKCcvKicgKyBsZWZ0ICsgbm9kZS50ZXh0ICsgcmlnaHQgKyAnKi8nLCBub2RlKTtcbiAgICAgICAgfVxuICAgIH1cbn1cblxuXG5leHBvcnQgZGVmYXVsdCBTYXNzU3RyaW5naWZpZXI7XG4iXX0=