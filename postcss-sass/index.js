'use strict';

exports.__esModule = true;

var _parse = require('./parse');

var _parse2 = _interopRequireDefault(_parse);

var _stringify = require('./stringify');

var _stringify2 = _interopRequireDefault(_stringify);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

exports.default = {
    parse: _parse2.default,
    stringify: _stringify2.default
};
module.exports = exports['default'];
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImluZGV4LmVzNiJdLCJuYW1lcyI6WyJwYXJzZSIsInN0cmluZ2lmeSJdLCJtYXBwaW5ncyI6Ijs7OztBQUFBOzs7O0FBQ0E7Ozs7OztrQkFDZTtBQUNYQSwwQkFEVztBQUVYQztBQUZXLEMiLCJmaWxlIjoiaW5kZXguanMiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgcGFyc2UgZnJvbSAnLi9wYXJzZSc7XG5pbXBvcnQgc3RyaW5naWZ5IGZyb20gJy4vc3RyaW5naWZ5JztcbmV4cG9ydCBkZWZhdWx0IHtcbiAgICBwYXJzZSxcbiAgICBzdHJpbmdpZnlcbn07XG4iXX0=