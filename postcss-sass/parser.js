'use strict';

exports.__esModule = true;

var _postcss = require('postcss');

var _postcss2 = _interopRequireDefault(_postcss);

var _gonzalesPe = require('gonzales-pe');

var _gonzalesPe2 = _interopRequireDefault(_gonzalesPe);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

var DEFAULT_RAWS_ROOT = {
    before: ''
};

var DEFAULT_RAWS_RULE = {
    before: '',
    between: ''
};

var DEFAULT_RAWS_DECL = {
    before: '',
    between: '',
    semicolon: false
};

var DEFAULT_COMMENT_DECL = {
    before: '',
    left: '',
    right: ''
};

var SassParser = function () {
    function SassParser(input) {
        _classCallCheck(this, SassParser);

        this.input = input;
    }

    SassParser.prototype.parse = function parse() {
        try {
            this.node = _gonzalesPe2.default.parse(this.input.css, { syntax: 'sass' });
        } catch (error) {
            throw this.input.error(error.message, error.line, 1);
        }
        this.lines = this.input.css.match(/^.*(\r?\n|$)/gm);
        this.root = this.stylesheet(this.node);
    };

    SassParser.prototype.extractSource = function extractSource(start, end) {
        var nodeLines = this.lines.slice(start.line - 1, end.line);

        nodeLines[0] = nodeLines[0].substring(start.column - 1);
        var last = nodeLines.length - 1;
        nodeLines[last] = nodeLines[last].substring(0, end.column);

        return nodeLines.join('');
    };

    SassParser.prototype.stylesheet = function stylesheet(node) {
        var _this = this;

        // Create and set parameters for Root node
        var root = _postcss2.default.root();
        root.source = {
            start: node.start,
            end: node.end,
            input: this.input
        };
        // Raws for root node
        root.raws = {
            semicolon: DEFAULT_RAWS_ROOT.semicolon,
            before: DEFAULT_RAWS_ROOT.before
        };
        // Store spaces before root (if exist)
        this.raws = {
            before: ''
        };
        node.content.forEach(function (contentNode) {
            return _this.process(contentNode, root);
        });
        return root;
    };

    SassParser.prototype.process = function process(node, parent) {
        if (this[node.type]) {
            return this[node.type](node, parent) || null;
        } else {
            return null;
        }
    };

    SassParser.prototype.ruleset = function ruleset(node, parent) {
        var _this2 = this;

        // Loop to find the deepest ruleset node
        this.raws.multiRuleProp = '';

        node.content.forEach(function (contentNode) {
            switch (contentNode.type) {
                case 'block':
                    {
                        // Create Rule node
                        var rule = _postcss2.default.rule();
                        rule.selector = '';
                        // Object to store raws for Rule
                        var ruleRaws = {
                            before: _this2.raws.before || DEFAULT_RAWS_RULE.before,
                            between: DEFAULT_RAWS_RULE.between
                        };

                        // Variable to store spaces and symbols before declaration property
                        _this2.raws.before = '';
                        _this2.raws.comment = false;

                        // Look up throw all nodes in current ruleset node
                        node.content.filter(function (_ref) {
                            var type = _ref.type;
                            return type === 'block';
                        }).forEach(function (innerContentNode) {
                            return _this2.process(innerContentNode, rule);
                        });

                        if (rule.nodes.length) {
                            // Write selector to Rule
                            rule.selector = _this2.extractSource(node.start, contentNode.start).slice(0, -1).replace(/\s+$/, function (spaces) {
                                ruleRaws.between = spaces;
                                return '';
                            });
                            // Set parameters for Rule node
                            rule.parent = parent;
                            rule.source = {
                                start: node.start,
                                end: node.end,
                                input: _this2.input
                            };
                            rule.raws = ruleRaws;
                            parent.nodes.push(rule);
                        }
                        break;
                    }
                default:
            }
        });
    };

    SassParser.prototype.block = function block(node, parent) {
        var _this3 = this;

        // If nested rules exist, wrap current rule in new rule node
        if (this.raws.multiRule) {
            if (this.raws.multiRulePropVariable) {
                this.raws.multiRuleProp = '$' + this.raws.multiRuleProp;
            }
            var multiRule = Object.assign(_postcss2.default.rule(), {
                source: {
                    start: {
                        line: node.start.line - 1,
                        column: node.start.column
                    },
                    end: node.end,
                    input: this.input
                },
                raws: {
                    before: this.raws.before || DEFAULT_RAWS_RULE.before,
                    between: DEFAULT_RAWS_RULE.between
                },
                parent: parent,
                selector: (this.raws.customProperty ? '--' : '') + this.raws.multiRuleProp
            });
            parent.push(multiRule);
            parent = multiRule;
        }

        this.raws.before = '';

        // Looking for declaration node in block node
        node.content.forEach(function (contentNode) {
            return _this3.process(contentNode, parent);
        });
        if (this.raws.multiRule) {
            this.raws.beforeMulti = this.raws.before;
        }
    };

    SassParser.prototype.declaration = function declaration(node, parent) {
        var _this4 = this;

        var isBlockInside = false;
        // Create Declaration node
        var declarationNode = _postcss2.default.decl();
        declarationNode.prop = '';

        // Object to store raws for Declaration
        var declarationRaws = Object.assign(declarationNode.raws, {
            before: this.raws.before || DEFAULT_RAWS_DECL.before,
            between: DEFAULT_RAWS_DECL.between,
            semicolon: DEFAULT_RAWS_DECL.semicolon
        });

        this.raws.property = false;
        this.raws.betweenBefore = false;
        this.raws.comment = false;
        // Looking for property and value node in declaration node
        node.content.forEach(function (contentNode) {
            switch (contentNode.type) {
                case 'customProperty':
                    _this4.raws.customProperty = true;
                // fall through
                case 'property':
                    {
                        /* global.property to detect is property is already defined in current object */
                        _this4.raws.property = true;
                        _this4.raws.multiRuleProp = contentNode.content[0].content;
                        _this4.raws.multiRulePropVariable = contentNode.content[0].type === 'variable';
                        _this4.process(contentNode, declarationNode);
                        break;
                    }
                case 'propertyDelimiter':
                    {
                        if (_this4.raws.property && !_this4.raws.betweenBefore) {
                            /* If property is already defined and there's no ':' before it */
                            declarationRaws.between += contentNode.content;
                            _this4.raws.multiRuleProp += contentNode.content;
                        } else {
                            /* If ':' goes before property declaration, like :width 100px */
                            _this4.raws.betweenBefore = true;
                            declarationRaws.before += contentNode.content;
                            _this4.raws.multiRuleProp += contentNode.content;
                        }
                        break;
                    }
                case 'space':
                    {
                        declarationRaws.between += contentNode.content;
                        break;
                    }
                case 'value':
                    {
                        // Look up for a value for current property
                        switch (contentNode.content[0].type) {
                            case 'block':
                                {
                                    isBlockInside = true;
                                    // If nested rules exist
                                    if (Array.isArray(contentNode.content[0].content)) {
                                        _this4.raws.multiRule = true;
                                    }
                                    _this4.process(contentNode.content[0], parent);
                                    break;
                                }
                            case 'variable':
                                {
                                    declarationNode.value = '$';
                                    _this4.process(contentNode, declarationNode);
                                    break;
                                }
                            case 'color':
                                {
                                    declarationNode.value = '#';
                                    _this4.process(contentNode, declarationNode);
                                    break;
                                }
                            case 'number':
                                {
                                    if (contentNode.content.length > 1) {
                                        declarationNode.value = contentNode.content.join('');
                                    } else {
                                        _this4.process(contentNode, declarationNode);
                                    }
                                    break;
                                }
                            case 'parentheses':
                                {
                                    declarationNode.value = '(';
                                    _this4.process(contentNode, declarationNode);
                                    break;
                                }
                            default:
                                {
                                    _this4.process(contentNode, declarationNode);
                                }
                        }
                        break;
                    }
                default:
            }
        });

        if (!isBlockInside) {
            // Set parameters for Declaration node
            declarationNode.source = {
                start: node.start,
                end: node.end,
                input: this.input
            };
            declarationNode.parent = parent;
            parent.nodes.push(declarationNode);
        }

        this.raws.before = '';
        this.raws.customProperty = false;
        this.raws.multiRuleProp = '';
        this.raws.property = false;
    };

    SassParser.prototype.customProperty = function customProperty(node, parent) {
        this.property(node, parent);
        parent.prop = '--' + parent.prop;
    };

    SassParser.prototype.property = function property(node, parent) {
        // Set property for Declaration node
        switch (node.content[0].type) {
            case 'variable':
                {
                    parent.prop += '$';
                    break;
                }
            case 'interpolation':
                {
                    this.raws.interpolation = true;
                    parent.prop += '#{';
                    break;
                }
            default:
        }
        parent.prop += node.content[0].content;
        if (this.raws.interpolation) {
            parent.prop += '}';
            this.raws.interpolation = false;
        }
    };

    SassParser.prototype.value = function value(node, parent) {
        if (!parent.value) {
            parent.value = '';
        }
        // Set value for Declaration node
        if (node.content.length) {
            node.content.forEach(function (contentNode) {
                switch (contentNode.type) {
                    case 'important':
                        {
                            parent.raws.important = contentNode.content;
                            parent.important = true;
                            var match = parent.value.match(/^(.*?)(\s*)$/);
                            if (match) {
                                parent.raws.important = match[2] + parent.raws.important;
                                parent.value = match[1];
                            }
                            break;
                        }
                    case 'parentheses':
                        {
                            parent.value += contentNode.content.join('') + ')';
                            break;
                        }
                    case 'percentage':
                        {
                            parent.value += contentNode.content.join('') + '%';
                            break;
                        }
                    default:
                        {
                            if (contentNode.content.constructor === Array) {
                                parent.value += contentNode.content.join('');
                            } else {
                                parent.value += contentNode.content;
                            }
                        }
                }
            });
        }
    };

    SassParser.prototype.singlelineComment = function singlelineComment(node, parent) {
        return this.comment(node, parent, true);
    };

    SassParser.prototype.multilineComment = function multilineComment(node, parent) {
        return this.comment(node, parent, false);
    };

    SassParser.prototype.comment = function comment(node, parent, inline) {
        var text = node.content.match(/^(\s*)((?:\S[\s\S]*?)?)(\s*)$/);

        this.raws.comment = true;

        var comment = Object.assign(_postcss2.default.comment(), {
            text: text[2],
            raws: {
                before: this.raws.before || DEFAULT_COMMENT_DECL.before,
                left: text[1],
                right: text[3],
                inline: inline
            }
        });

        if (this.raws.beforeMulti) {
            comment.raws.before += this.raws.beforeMulti;
            this.raws.beforeMulti = undefined;
        }

        parent.nodes.push(comment);
        this.raws.before = '';
    };

    SassParser.prototype.space = function space(node, parent) {
        // Spaces before root and rule
        switch (parent.type) {
            case 'root':
                {
                    this.raws.before += node.content;
                    break;
                }
            case 'rule':
                {
                    if (this.raws.comment) {
                        this.raws.before += node.content;
                    } else if (this.raws.loop) {
                        parent.selector += node.content;
                    } else {
                        this.raws.before = (this.raws.before || '\n') + node.content;
                    }
                    break;
                }
            default:
        }
    };

    SassParser.prototype.declarationDelimiter = function declarationDelimiter(node) {
        this.raws.before += node.content;
    };

    SassParser.prototype.loop = function loop(node, parent) {
        var _this5 = this;

        var loop = _postcss2.default.rule();
        this.raws.comment = false;
        this.raws.multiRule = false;
        this.raws.loop = true;
        loop.selector = '';
        loop.raws = {
            before: this.raws.before || DEFAULT_RAWS_RULE.before,
            between: DEFAULT_RAWS_RULE.between
        };
        if (this.raws.beforeMulti) {
            loop.raws.before += this.raws.beforeMulti;
            this.raws.beforeMulti = undefined;
        }
        node.content.forEach(function (contentNode, i) {
            if (node.content[i + 1] && node.content[i + 1].type === 'block') {
                _this5.raws.loop = false;
            }
            _this5.process(contentNode, loop);
        });
        parent.nodes.push(loop);
        this.raws.loop = false;
    };

    SassParser.prototype.atkeyword = function atkeyword(node, parent) {
        parent.selector += '@' + node.content;
    };

    SassParser.prototype.operator = function operator(node, parent) {
        parent.selector += node.content;
    };

    SassParser.prototype.variable = function variable(node, parent) {
        if (this.raws.loop) {
            parent.selector += '$' + node.content[0].content;
        } else {
            parent.selector += '#' + node.content[0].content;
        }
    };

    SassParser.prototype.ident = function ident(node, parent) {
        parent.selector += node.content;
    };

    return SassParser;
}();

exports.default = SassParser;
module.exports = exports['default'];
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInBhcnNlci5lczYiXSwibmFtZXMiOlsiREVGQVVMVF9SQVdTX1JPT1QiLCJiZWZvcmUiLCJERUZBVUxUX1JBV1NfUlVMRSIsImJldHdlZW4iLCJERUZBVUxUX1JBV1NfREVDTCIsInNlbWljb2xvbiIsIkRFRkFVTFRfQ09NTUVOVF9ERUNMIiwibGVmdCIsInJpZ2h0IiwiU2Fzc1BhcnNlciIsImlucHV0IiwicGFyc2UiLCJub2RlIiwiY3NzIiwic3ludGF4IiwiZXJyb3IiLCJtZXNzYWdlIiwibGluZSIsImxpbmVzIiwibWF0Y2giLCJyb290Iiwic3R5bGVzaGVldCIsImV4dHJhY3RTb3VyY2UiLCJzdGFydCIsImVuZCIsIm5vZGVMaW5lcyIsInNsaWNlIiwic3Vic3RyaW5nIiwiY29sdW1uIiwibGFzdCIsImxlbmd0aCIsImpvaW4iLCJzb3VyY2UiLCJyYXdzIiwiY29udGVudCIsImZvckVhY2giLCJwcm9jZXNzIiwiY29udGVudE5vZGUiLCJwYXJlbnQiLCJ0eXBlIiwicnVsZXNldCIsIm11bHRpUnVsZVByb3AiLCJydWxlIiwic2VsZWN0b3IiLCJydWxlUmF3cyIsImNvbW1lbnQiLCJmaWx0ZXIiLCJpbm5lckNvbnRlbnROb2RlIiwibm9kZXMiLCJyZXBsYWNlIiwic3BhY2VzIiwicHVzaCIsImJsb2NrIiwibXVsdGlSdWxlIiwibXVsdGlSdWxlUHJvcFZhcmlhYmxlIiwiT2JqZWN0IiwiYXNzaWduIiwiY3VzdG9tUHJvcGVydHkiLCJiZWZvcmVNdWx0aSIsImRlY2xhcmF0aW9uIiwiaXNCbG9ja0luc2lkZSIsImRlY2xhcmF0aW9uTm9kZSIsImRlY2wiLCJwcm9wIiwiZGVjbGFyYXRpb25SYXdzIiwicHJvcGVydHkiLCJiZXR3ZWVuQmVmb3JlIiwiQXJyYXkiLCJpc0FycmF5IiwidmFsdWUiLCJpbnRlcnBvbGF0aW9uIiwiaW1wb3J0YW50IiwiY29uc3RydWN0b3IiLCJzaW5nbGVsaW5lQ29tbWVudCIsIm11bHRpbGluZUNvbW1lbnQiLCJpbmxpbmUiLCJ0ZXh0IiwidW5kZWZpbmVkIiwic3BhY2UiLCJsb29wIiwiZGVjbGFyYXRpb25EZWxpbWl0ZXIiLCJpIiwiYXRrZXl3b3JkIiwib3BlcmF0b3IiLCJ2YXJpYWJsZSIsImlkZW50Il0sIm1hcHBpbmdzIjoiOzs7O0FBQUE7Ozs7QUFDQTs7Ozs7Ozs7QUFFQSxJQUFNQSxvQkFBb0I7QUFDdEJDLFlBQVE7QUFEYyxDQUExQjs7QUFJQSxJQUFNQyxvQkFBb0I7QUFDdEJELFlBQVEsRUFEYztBQUV0QkUsYUFBUztBQUZhLENBQTFCOztBQUtBLElBQU1DLG9CQUFvQjtBQUN0QkgsWUFBUSxFQURjO0FBRXRCRSxhQUFTLEVBRmE7QUFHdEJFLGVBQVc7QUFIVyxDQUExQjs7QUFNQSxJQUFNQyx1QkFBdUI7QUFDekJMLFlBQVEsRUFEaUI7QUFFekJNLFVBQU0sRUFGbUI7QUFHekJDLFdBQU87QUFIa0IsQ0FBN0I7O0lBTU1DLFU7QUFDRix3QkFBWUMsS0FBWixFQUFtQjtBQUFBOztBQUNmLGFBQUtBLEtBQUwsR0FBYUEsS0FBYjtBQUNIOzt5QkFDREMsSyxvQkFBUTtBQUNKLFlBQUk7QUFDQSxpQkFBS0MsSUFBTCxHQUFZLHFCQUFTRCxLQUFULENBQWUsS0FBS0QsS0FBTCxDQUFXRyxHQUExQixFQUErQixFQUFFQyxRQUFRLE1BQVYsRUFBL0IsQ0FBWjtBQUNILFNBRkQsQ0FFRSxPQUFPQyxLQUFQLEVBQWM7QUFDWixrQkFBTSxLQUFLTCxLQUFMLENBQVdLLEtBQVgsQ0FBaUJBLE1BQU1DLE9BQXZCLEVBQWdDRCxNQUFNRSxJQUF0QyxFQUE0QyxDQUE1QyxDQUFOO0FBQ0g7QUFDRCxhQUFLQyxLQUFMLEdBQWEsS0FBS1IsS0FBTCxDQUFXRyxHQUFYLENBQWVNLEtBQWYsQ0FBcUIsZ0JBQXJCLENBQWI7QUFDQSxhQUFLQyxJQUFMLEdBQVksS0FBS0MsVUFBTCxDQUFnQixLQUFLVCxJQUFyQixDQUFaO0FBQ0gsSzs7eUJBQ0RVLGEsMEJBQWNDLEssRUFBT0MsRyxFQUFLO0FBQ3RCLFlBQU1DLFlBQVksS0FBS1AsS0FBTCxDQUFXUSxLQUFYLENBQ2RILE1BQU1OLElBQU4sR0FBYSxDQURDLEVBRWRPLElBQUlQLElBRlUsQ0FBbEI7O0FBS0FRLGtCQUFVLENBQVYsSUFBZUEsVUFBVSxDQUFWLEVBQWFFLFNBQWIsQ0FBdUJKLE1BQU1LLE1BQU4sR0FBZSxDQUF0QyxDQUFmO0FBQ0EsWUFBTUMsT0FBT0osVUFBVUssTUFBVixHQUFtQixDQUFoQztBQUNBTCxrQkFBVUksSUFBVixJQUFrQkosVUFBVUksSUFBVixFQUFnQkYsU0FBaEIsQ0FBMEIsQ0FBMUIsRUFBNkJILElBQUlJLE1BQWpDLENBQWxCOztBQUVBLGVBQU9ILFVBQVVNLElBQVYsQ0FBZSxFQUFmLENBQVA7QUFDSCxLOzt5QkFDRFYsVSx1QkFBV1QsSSxFQUFNO0FBQUE7O0FBQ2I7QUFDQSxZQUFNUSxPQUFPLGtCQUFRQSxJQUFSLEVBQWI7QUFDQUEsYUFBS1ksTUFBTCxHQUFjO0FBQ1ZULG1CQUFPWCxLQUFLVyxLQURGO0FBRVZDLGlCQUFLWixLQUFLWSxHQUZBO0FBR1ZkLG1CQUFPLEtBQUtBO0FBSEYsU0FBZDtBQUtBO0FBQ0FVLGFBQUthLElBQUwsR0FBWTtBQUNSNUIsdUJBQVdMLGtCQUFrQkssU0FEckI7QUFFUkosb0JBQVFELGtCQUFrQkM7QUFGbEIsU0FBWjtBQUlBO0FBQ0EsYUFBS2dDLElBQUwsR0FBWTtBQUNSaEMsb0JBQVE7QUFEQSxTQUFaO0FBR0FXLGFBQUtzQixPQUFMLENBQWFDLE9BQWIsQ0FBcUI7QUFBQSxtQkFBZSxNQUFLQyxPQUFMLENBQWFDLFdBQWIsRUFBMEJqQixJQUExQixDQUFmO0FBQUEsU0FBckI7QUFDQSxlQUFPQSxJQUFQO0FBQ0gsSzs7eUJBQ0RnQixPLG9CQUFReEIsSSxFQUFNMEIsTSxFQUFRO0FBQ2xCLFlBQUksS0FBSzFCLEtBQUsyQixJQUFWLENBQUosRUFBcUI7QUFDakIsbUJBQU8sS0FBSzNCLEtBQUsyQixJQUFWLEVBQWdCM0IsSUFBaEIsRUFBc0IwQixNQUF0QixLQUFpQyxJQUF4QztBQUNILFNBRkQsTUFFTztBQUNILG1CQUFPLElBQVA7QUFDSDtBQUNKLEs7O3lCQUNERSxPLG9CQUFRNUIsSSxFQUFNMEIsTSxFQUFRO0FBQUE7O0FBQ2xCO0FBQ0EsYUFBS0wsSUFBTCxDQUFVUSxhQUFWLEdBQTBCLEVBQTFCOztBQUVBN0IsYUFBS3NCLE9BQUwsQ0FBYUMsT0FBYixDQUFxQix1QkFBZTtBQUNoQyxvQkFBUUUsWUFBWUUsSUFBcEI7QUFDSSxxQkFBSyxPQUFMO0FBQWM7QUFDVjtBQUNBLDRCQUFNRyxPQUFPLGtCQUFRQSxJQUFSLEVBQWI7QUFDQUEsNkJBQUtDLFFBQUwsR0FBZ0IsRUFBaEI7QUFDQTtBQUNBLDRCQUFNQyxXQUFXO0FBQ2IzQyxvQ0FBUSxPQUFLZ0MsSUFBTCxDQUFVaEMsTUFBVixJQUFvQkMsa0JBQWtCRCxNQURqQztBQUViRSxxQ0FBU0Qsa0JBQWtCQztBQUZkLHlCQUFqQjs7QUFLQTtBQUNBLCtCQUFLOEIsSUFBTCxDQUFVaEMsTUFBVixHQUFtQixFQUFuQjtBQUNBLCtCQUFLZ0MsSUFBTCxDQUFVWSxPQUFWLEdBQW9CLEtBQXBCOztBQUVBO0FBQ0FqQyw2QkFBS3NCLE9BQUwsQ0FDS1ksTUFETCxDQUNZO0FBQUEsZ0NBQUdQLElBQUgsUUFBR0EsSUFBSDtBQUFBLG1DQUFjQSxTQUFTLE9BQXZCO0FBQUEseUJBRFosRUFFS0osT0FGTCxDQUVhO0FBQUEsbUNBQW9CLE9BQUtDLE9BQUwsQ0FBYVcsZ0JBQWIsRUFBK0JMLElBQS9CLENBQXBCO0FBQUEseUJBRmI7O0FBSUEsNEJBQUlBLEtBQUtNLEtBQUwsQ0FBV2xCLE1BQWYsRUFBdUI7QUFDbkI7QUFDQVksaUNBQUtDLFFBQUwsR0FBZ0IsT0FBS3JCLGFBQUwsQ0FDWlYsS0FBS1csS0FETyxFQUVaYyxZQUFZZCxLQUZBLEVBR2RHLEtBSGMsQ0FHUixDQUhRLEVBR0wsQ0FBQyxDQUhJLEVBR0R1QixPQUhDLENBR08sTUFIUCxFQUdlLGtCQUFVO0FBQ3JDTCx5Q0FBU3pDLE9BQVQsR0FBbUIrQyxNQUFuQjtBQUNBLHVDQUFPLEVBQVA7QUFDSCw2QkFOZSxDQUFoQjtBQU9BO0FBQ0FSLGlDQUFLSixNQUFMLEdBQWNBLE1BQWQ7QUFDQUksaUNBQUtWLE1BQUwsR0FBYztBQUNWVCx1Q0FBT1gsS0FBS1csS0FERjtBQUVWQyxxQ0FBS1osS0FBS1ksR0FGQTtBQUdWZCx1Q0FBTyxPQUFLQTtBQUhGLDZCQUFkO0FBS0FnQyxpQ0FBS1QsSUFBTCxHQUFZVyxRQUFaO0FBQ0FOLG1DQUFPVSxLQUFQLENBQWFHLElBQWIsQ0FBa0JULElBQWxCO0FBQ0g7QUFDRDtBQUNIO0FBQ0Q7QUF6Q0o7QUEyQ0gsU0E1Q0Q7QUE2Q0gsSzs7eUJBQ0RVLEssa0JBQU14QyxJLEVBQU0wQixNLEVBQVE7QUFBQTs7QUFDaEI7QUFDQSxZQUFJLEtBQUtMLElBQUwsQ0FBVW9CLFNBQWQsRUFBeUI7QUFDckIsZ0JBQUksS0FBS3BCLElBQUwsQ0FBVXFCLHFCQUFkLEVBQXFDO0FBQ2pDLHFCQUFLckIsSUFBTCxDQUFVUSxhQUFWLFNBQStCLEtBQUtSLElBQUwsQ0FBVVEsYUFBekM7QUFDSDtBQUNELGdCQUFNWSxZQUFZRSxPQUFPQyxNQUFQLENBQWMsa0JBQVFkLElBQVIsRUFBZCxFQUE4QjtBQUM1Q1Ysd0JBQVE7QUFDSlQsMkJBQU87QUFDSE4sOEJBQU1MLEtBQUtXLEtBQUwsQ0FBV04sSUFBWCxHQUFrQixDQURyQjtBQUVIVyxnQ0FBUWhCLEtBQUtXLEtBQUwsQ0FBV0s7QUFGaEIscUJBREg7QUFLSkoseUJBQUtaLEtBQUtZLEdBTE47QUFNSmQsMkJBQU8sS0FBS0E7QUFOUixpQkFEb0M7QUFTNUN1QixzQkFBTTtBQUNGaEMsNEJBQVEsS0FBS2dDLElBQUwsQ0FBVWhDLE1BQVYsSUFBb0JDLGtCQUFrQkQsTUFENUM7QUFFRkUsNkJBQVNELGtCQUFrQkM7QUFGekIsaUJBVHNDO0FBYTVDbUMsOEJBYjRDO0FBYzVDSywwQkFBVSxDQUFDLEtBQUtWLElBQUwsQ0FBVXdCLGNBQVYsR0FBMkIsSUFBM0IsR0FBa0MsRUFBbkMsSUFBeUMsS0FBS3hCLElBQUwsQ0FBVVE7QUFkakIsYUFBOUIsQ0FBbEI7QUFnQkFILG1CQUFPYSxJQUFQLENBQVlFLFNBQVo7QUFDQWYscUJBQVNlLFNBQVQ7QUFDSDs7QUFFRCxhQUFLcEIsSUFBTCxDQUFVaEMsTUFBVixHQUFtQixFQUFuQjs7QUFFQTtBQUNBVyxhQUFLc0IsT0FBTCxDQUFhQyxPQUFiLENBQXFCO0FBQUEsbUJBQWUsT0FBS0MsT0FBTCxDQUFhQyxXQUFiLEVBQTBCQyxNQUExQixDQUFmO0FBQUEsU0FBckI7QUFDQSxZQUFJLEtBQUtMLElBQUwsQ0FBVW9CLFNBQWQsRUFBeUI7QUFDckIsaUJBQUtwQixJQUFMLENBQVV5QixXQUFWLEdBQXdCLEtBQUt6QixJQUFMLENBQVVoQyxNQUFsQztBQUNIO0FBQ0osSzs7eUJBQ0QwRCxXLHdCQUFZL0MsSSxFQUFNMEIsTSxFQUFRO0FBQUE7O0FBQ3RCLFlBQUlzQixnQkFBZ0IsS0FBcEI7QUFDQTtBQUNBLFlBQU1DLGtCQUFrQixrQkFBUUMsSUFBUixFQUF4QjtBQUNBRCx3QkFBZ0JFLElBQWhCLEdBQXVCLEVBQXZCOztBQUVBO0FBQ0EsWUFBTUMsa0JBQWtCVCxPQUFPQyxNQUFQLENBQWNLLGdCQUFnQjVCLElBQTlCLEVBQW9DO0FBQ3hEaEMsb0JBQVEsS0FBS2dDLElBQUwsQ0FBVWhDLE1BQVYsSUFBb0JHLGtCQUFrQkgsTUFEVTtBQUV4REUscUJBQVNDLGtCQUFrQkQsT0FGNkI7QUFHeERFLHVCQUFXRCxrQkFBa0JDO0FBSDJCLFNBQXBDLENBQXhCOztBQU1BLGFBQUs0QixJQUFMLENBQVVnQyxRQUFWLEdBQXFCLEtBQXJCO0FBQ0EsYUFBS2hDLElBQUwsQ0FBVWlDLGFBQVYsR0FBMEIsS0FBMUI7QUFDQSxhQUFLakMsSUFBTCxDQUFVWSxPQUFWLEdBQW9CLEtBQXBCO0FBQ0E7QUFDQWpDLGFBQUtzQixPQUFMLENBQWFDLE9BQWIsQ0FBcUIsVUFBQ0UsV0FBRCxFQUFpQjtBQUNsQyxvQkFBUUEsWUFBWUUsSUFBcEI7QUFDSSxxQkFBSyxnQkFBTDtBQUNJLDJCQUFLTixJQUFMLENBQVV3QixjQUFWLEdBQTJCLElBQTNCO0FBQ0E7QUFDSixxQkFBSyxVQUFMO0FBQWlCO0FBQ2I7QUFDQSwrQkFBS3hCLElBQUwsQ0FBVWdDLFFBQVYsR0FBcUIsSUFBckI7QUFDQSwrQkFBS2hDLElBQUwsQ0FBVVEsYUFBVixHQUEwQkosWUFBWUgsT0FBWixDQUFvQixDQUFwQixFQUF1QkEsT0FBakQ7QUFDQSwrQkFBS0QsSUFBTCxDQUFVcUIscUJBQVYsR0FBa0NqQixZQUFZSCxPQUFaLENBQW9CLENBQXBCLEVBQXVCSyxJQUF2QixLQUFnQyxVQUFsRTtBQUNBLCtCQUFLSCxPQUFMLENBQWFDLFdBQWIsRUFBMEJ3QixlQUExQjtBQUNBO0FBQ0g7QUFDRCxxQkFBSyxtQkFBTDtBQUEwQjtBQUN0Qiw0QkFBSSxPQUFLNUIsSUFBTCxDQUFVZ0MsUUFBVixJQUFzQixDQUFDLE9BQUtoQyxJQUFMLENBQVVpQyxhQUFyQyxFQUFvRDtBQUNoRDtBQUNBRiw0Q0FBZ0I3RCxPQUFoQixJQUEyQmtDLFlBQVlILE9BQXZDO0FBQ0EsbUNBQUtELElBQUwsQ0FBVVEsYUFBVixJQUEyQkosWUFBWUgsT0FBdkM7QUFDSCx5QkFKRCxNQUlPO0FBQ0g7QUFDQSxtQ0FBS0QsSUFBTCxDQUFVaUMsYUFBVixHQUEwQixJQUExQjtBQUNBRiw0Q0FBZ0IvRCxNQUFoQixJQUEwQm9DLFlBQVlILE9BQXRDO0FBQ0EsbUNBQUtELElBQUwsQ0FBVVEsYUFBVixJQUEyQkosWUFBWUgsT0FBdkM7QUFDSDtBQUNEO0FBQ0g7QUFDRCxxQkFBSyxPQUFMO0FBQWM7QUFDVjhCLHdDQUFnQjdELE9BQWhCLElBQTJCa0MsWUFBWUgsT0FBdkM7QUFDQTtBQUNIO0FBQ0QscUJBQUssT0FBTDtBQUFjO0FBQ1Y7QUFDQSxnQ0FBUUcsWUFBWUgsT0FBWixDQUFvQixDQUFwQixFQUF1QkssSUFBL0I7QUFDSSxpQ0FBSyxPQUFMO0FBQWM7QUFDVnFCLG9EQUFnQixJQUFoQjtBQUNBO0FBQ0Esd0NBQUlPLE1BQU1DLE9BQU4sQ0FBYy9CLFlBQVlILE9BQVosQ0FBb0IsQ0FBcEIsRUFBdUJBLE9BQXJDLENBQUosRUFBbUQ7QUFDL0MsK0NBQUtELElBQUwsQ0FBVW9CLFNBQVYsR0FBc0IsSUFBdEI7QUFDSDtBQUNELDJDQUFLakIsT0FBTCxDQUFhQyxZQUFZSCxPQUFaLENBQW9CLENBQXBCLENBQWIsRUFBcUNJLE1BQXJDO0FBQ0E7QUFDSDtBQUNELGlDQUFLLFVBQUw7QUFBaUI7QUFDYnVCLG9EQUFnQlEsS0FBaEIsR0FBd0IsR0FBeEI7QUFDQSwyQ0FBS2pDLE9BQUwsQ0FBYUMsV0FBYixFQUEwQndCLGVBQTFCO0FBQ0E7QUFDSDtBQUNELGlDQUFLLE9BQUw7QUFBYztBQUNWQSxvREFBZ0JRLEtBQWhCLEdBQXdCLEdBQXhCO0FBQ0EsMkNBQUtqQyxPQUFMLENBQWFDLFdBQWIsRUFBMEJ3QixlQUExQjtBQUNBO0FBQ0g7QUFDRCxpQ0FBSyxRQUFMO0FBQWU7QUFDWCx3Q0FBSXhCLFlBQVlILE9BQVosQ0FBb0JKLE1BQXBCLEdBQTZCLENBQWpDLEVBQW9DO0FBQ2hDK0Isd0RBQWdCUSxLQUFoQixHQUF3QmhDLFlBQVlILE9BQVosQ0FBb0JILElBQXBCLENBQXlCLEVBQXpCLENBQXhCO0FBQ0gscUNBRkQsTUFFTztBQUNILCtDQUFLSyxPQUFMLENBQWFDLFdBQWIsRUFBMEJ3QixlQUExQjtBQUNIO0FBQ0Q7QUFDSDtBQUNELGlDQUFLLGFBQUw7QUFBb0I7QUFDaEJBLG9EQUFnQlEsS0FBaEIsR0FBd0IsR0FBeEI7QUFDQSwyQ0FBS2pDLE9BQUwsQ0FBYUMsV0FBYixFQUEwQndCLGVBQTFCO0FBQ0E7QUFDSDtBQUNEO0FBQVM7QUFDTCwyQ0FBS3pCLE9BQUwsQ0FBYUMsV0FBYixFQUEwQndCLGVBQTFCO0FBQ0g7QUFuQ0w7QUFxQ0E7QUFDSDtBQUNEO0FBdEVKO0FBd0VILFNBekVEOztBQTJFQSxZQUFJLENBQUNELGFBQUwsRUFBb0I7QUFDaEI7QUFDQUMsNEJBQWdCN0IsTUFBaEIsR0FBeUI7QUFDckJULHVCQUFPWCxLQUFLVyxLQURTO0FBRXJCQyxxQkFBS1osS0FBS1ksR0FGVztBQUdyQmQsdUJBQU8sS0FBS0E7QUFIUyxhQUF6QjtBQUtBbUQsNEJBQWdCdkIsTUFBaEIsR0FBeUJBLE1BQXpCO0FBQ0FBLG1CQUFPVSxLQUFQLENBQWFHLElBQWIsQ0FBa0JVLGVBQWxCO0FBQ0g7O0FBRUQsYUFBSzVCLElBQUwsQ0FBVWhDLE1BQVYsR0FBbUIsRUFBbkI7QUFDQSxhQUFLZ0MsSUFBTCxDQUFVd0IsY0FBVixHQUEyQixLQUEzQjtBQUNBLGFBQUt4QixJQUFMLENBQVVRLGFBQVYsR0FBMEIsRUFBMUI7QUFDQSxhQUFLUixJQUFMLENBQVVnQyxRQUFWLEdBQXFCLEtBQXJCO0FBQ0gsSzs7eUJBQ0RSLGMsMkJBQWU3QyxJLEVBQU0wQixNLEVBQVE7QUFDekIsYUFBSzJCLFFBQUwsQ0FBY3JELElBQWQsRUFBb0IwQixNQUFwQjtBQUNBQSxlQUFPeUIsSUFBUCxVQUFtQnpCLE9BQU95QixJQUExQjtBQUNILEs7O3lCQUNERSxRLHFCQUFTckQsSSxFQUFNMEIsTSxFQUFRO0FBQ25CO0FBQ0EsZ0JBQVExQixLQUFLc0IsT0FBTCxDQUFhLENBQWIsRUFBZ0JLLElBQXhCO0FBQ0ksaUJBQUssVUFBTDtBQUFpQjtBQUNiRCwyQkFBT3lCLElBQVAsSUFBZSxHQUFmO0FBQ0E7QUFDSDtBQUNELGlCQUFLLGVBQUw7QUFBc0I7QUFDbEIseUJBQUs5QixJQUFMLENBQVVxQyxhQUFWLEdBQTBCLElBQTFCO0FBQ0FoQywyQkFBT3lCLElBQVAsSUFBZSxJQUFmO0FBQ0E7QUFDSDtBQUNEO0FBVko7QUFZQXpCLGVBQU95QixJQUFQLElBQWVuRCxLQUFLc0IsT0FBTCxDQUFhLENBQWIsRUFBZ0JBLE9BQS9CO0FBQ0EsWUFBSSxLQUFLRCxJQUFMLENBQVVxQyxhQUFkLEVBQTZCO0FBQ3pCaEMsbUJBQU95QixJQUFQLElBQWUsR0FBZjtBQUNBLGlCQUFLOUIsSUFBTCxDQUFVcUMsYUFBVixHQUEwQixLQUExQjtBQUNIO0FBQ0osSzs7eUJBQ0RELEssa0JBQU16RCxJLEVBQU0wQixNLEVBQVE7QUFDaEIsWUFBSSxDQUFDQSxPQUFPK0IsS0FBWixFQUFtQjtBQUNmL0IsbUJBQU8rQixLQUFQLEdBQWUsRUFBZjtBQUNIO0FBQ0Q7QUFDQSxZQUFJekQsS0FBS3NCLE9BQUwsQ0FBYUosTUFBakIsRUFBeUI7QUFDckJsQixpQkFBS3NCLE9BQUwsQ0FBYUMsT0FBYixDQUFxQix1QkFBZTtBQUNoQyx3QkFBUUUsWUFBWUUsSUFBcEI7QUFDSSx5QkFBSyxXQUFMO0FBQWtCO0FBQ2RELG1DQUFPTCxJQUFQLENBQVlzQyxTQUFaLEdBQXdCbEMsWUFBWUgsT0FBcEM7QUFDQUksbUNBQU9pQyxTQUFQLEdBQW1CLElBQW5CO0FBQ0EsZ0NBQU1wRCxRQUFRbUIsT0FBTytCLEtBQVAsQ0FBYWxELEtBQWIsQ0FBbUIsY0FBbkIsQ0FBZDtBQUNBLGdDQUFJQSxLQUFKLEVBQVc7QUFDUG1CLHVDQUFPTCxJQUFQLENBQVlzQyxTQUFaLEdBQXdCcEQsTUFBTSxDQUFOLElBQVdtQixPQUFPTCxJQUFQLENBQVlzQyxTQUEvQztBQUNBakMsdUNBQU8rQixLQUFQLEdBQWVsRCxNQUFNLENBQU4sQ0FBZjtBQUNIO0FBQ0Q7QUFDSDtBQUNELHlCQUFLLGFBQUw7QUFBb0I7QUFDaEJtQixtQ0FBTytCLEtBQVAsSUFBZ0JoQyxZQUFZSCxPQUFaLENBQW9CSCxJQUFwQixDQUF5QixFQUF6QixJQUErQixHQUEvQztBQUNBO0FBQ0g7QUFDRCx5QkFBSyxZQUFMO0FBQW1CO0FBQ2ZPLG1DQUFPK0IsS0FBUCxJQUFnQmhDLFlBQVlILE9BQVosQ0FBb0JILElBQXBCLENBQXlCLEVBQXpCLElBQStCLEdBQS9DO0FBQ0E7QUFDSDtBQUNEO0FBQVM7QUFDTCxnQ0FBSU0sWUFBWUgsT0FBWixDQUFvQnNDLFdBQXBCLEtBQW9DTCxLQUF4QyxFQUErQztBQUMzQzdCLHVDQUFPK0IsS0FBUCxJQUFnQmhDLFlBQVlILE9BQVosQ0FBb0JILElBQXBCLENBQXlCLEVBQXpCLENBQWhCO0FBQ0gsNkJBRkQsTUFFTztBQUNITyx1Q0FBTytCLEtBQVAsSUFBZ0JoQyxZQUFZSCxPQUE1QjtBQUNIO0FBQ0o7QUF6Qkw7QUEyQkgsYUE1QkQ7QUE2Qkg7QUFDSixLOzt5QkFDRHVDLGlCLDhCQUFrQjdELEksRUFBTTBCLE0sRUFBUTtBQUM1QixlQUFPLEtBQUtPLE9BQUwsQ0FBYWpDLElBQWIsRUFBbUIwQixNQUFuQixFQUEyQixJQUEzQixDQUFQO0FBQ0gsSzs7eUJBQ0RvQyxnQiw2QkFBaUI5RCxJLEVBQU0wQixNLEVBQVE7QUFDM0IsZUFBTyxLQUFLTyxPQUFMLENBQWFqQyxJQUFiLEVBQW1CMEIsTUFBbkIsRUFBMkIsS0FBM0IsQ0FBUDtBQUNILEs7O3lCQUNETyxPLG9CQUFRakMsSSxFQUFNMEIsTSxFQUFRcUMsTSxFQUFRO0FBQzFCLFlBQU1DLE9BQU9oRSxLQUFLc0IsT0FBTCxDQUFhZixLQUFiLENBQW1CLCtCQUFuQixDQUFiOztBQUVBLGFBQUtjLElBQUwsQ0FBVVksT0FBVixHQUFvQixJQUFwQjs7QUFFQSxZQUFNQSxVQUFVVSxPQUFPQyxNQUFQLENBQWMsa0JBQVFYLE9BQVIsRUFBZCxFQUFpQztBQUM3QytCLGtCQUFNQSxLQUFLLENBQUwsQ0FEdUM7QUFFN0MzQyxrQkFBTTtBQUNGaEMsd0JBQVEsS0FBS2dDLElBQUwsQ0FBVWhDLE1BQVYsSUFBb0JLLHFCQUFxQkwsTUFEL0M7QUFFRk0sc0JBQU1xRSxLQUFLLENBQUwsQ0FGSjtBQUdGcEUsdUJBQU9vRSxLQUFLLENBQUwsQ0FITDtBQUlGRDtBQUpFO0FBRnVDLFNBQWpDLENBQWhCOztBQVVBLFlBQUksS0FBSzFDLElBQUwsQ0FBVXlCLFdBQWQsRUFBMkI7QUFDdkJiLG9CQUFRWixJQUFSLENBQWFoQyxNQUFiLElBQXVCLEtBQUtnQyxJQUFMLENBQVV5QixXQUFqQztBQUNBLGlCQUFLekIsSUFBTCxDQUFVeUIsV0FBVixHQUF3Qm1CLFNBQXhCO0FBQ0g7O0FBRUR2QyxlQUFPVSxLQUFQLENBQWFHLElBQWIsQ0FBa0JOLE9BQWxCO0FBQ0EsYUFBS1osSUFBTCxDQUFVaEMsTUFBVixHQUFtQixFQUFuQjtBQUNILEs7O3lCQUNENkUsSyxrQkFBTWxFLEksRUFBTTBCLE0sRUFBUTtBQUNoQjtBQUNBLGdCQUFRQSxPQUFPQyxJQUFmO0FBQ0ksaUJBQUssTUFBTDtBQUFhO0FBQ1QseUJBQUtOLElBQUwsQ0FBVWhDLE1BQVYsSUFBb0JXLEtBQUtzQixPQUF6QjtBQUNBO0FBQ0g7QUFDRCxpQkFBSyxNQUFMO0FBQWE7QUFDVCx3QkFBSSxLQUFLRCxJQUFMLENBQVVZLE9BQWQsRUFBdUI7QUFDbkIsNkJBQUtaLElBQUwsQ0FBVWhDLE1BQVYsSUFBb0JXLEtBQUtzQixPQUF6QjtBQUNILHFCQUZELE1BRU8sSUFBSSxLQUFLRCxJQUFMLENBQVU4QyxJQUFkLEVBQW9CO0FBQ3ZCekMsK0JBQU9LLFFBQVAsSUFBbUIvQixLQUFLc0IsT0FBeEI7QUFDSCxxQkFGTSxNQUVBO0FBQ0gsNkJBQUtELElBQUwsQ0FBVWhDLE1BQVYsR0FBbUIsQ0FBQyxLQUFLZ0MsSUFBTCxDQUFVaEMsTUFBVixJQUFvQixJQUFyQixJQUE2QlcsS0FBS3NCLE9BQXJEO0FBQ0g7QUFDRDtBQUNIO0FBQ0Q7QUFmSjtBQWlCSCxLOzt5QkFDRDhDLG9CLGlDQUFxQnBFLEksRUFBTTtBQUN2QixhQUFLcUIsSUFBTCxDQUFVaEMsTUFBVixJQUFvQlcsS0FBS3NCLE9BQXpCO0FBQ0gsSzs7eUJBQ0Q2QyxJLGlCQUFLbkUsSSxFQUFNMEIsTSxFQUFRO0FBQUE7O0FBQ2YsWUFBTXlDLE9BQU8sa0JBQVFyQyxJQUFSLEVBQWI7QUFDQSxhQUFLVCxJQUFMLENBQVVZLE9BQVYsR0FBb0IsS0FBcEI7QUFDQSxhQUFLWixJQUFMLENBQVVvQixTQUFWLEdBQXNCLEtBQXRCO0FBQ0EsYUFBS3BCLElBQUwsQ0FBVThDLElBQVYsR0FBaUIsSUFBakI7QUFDQUEsYUFBS3BDLFFBQUwsR0FBZ0IsRUFBaEI7QUFDQW9DLGFBQUs5QyxJQUFMLEdBQVk7QUFDUmhDLG9CQUFRLEtBQUtnQyxJQUFMLENBQVVoQyxNQUFWLElBQW9CQyxrQkFBa0JELE1BRHRDO0FBRVJFLHFCQUFTRCxrQkFBa0JDO0FBRm5CLFNBQVo7QUFJQSxZQUFJLEtBQUs4QixJQUFMLENBQVV5QixXQUFkLEVBQTJCO0FBQ3ZCcUIsaUJBQUs5QyxJQUFMLENBQVVoQyxNQUFWLElBQW9CLEtBQUtnQyxJQUFMLENBQVV5QixXQUE5QjtBQUNBLGlCQUFLekIsSUFBTCxDQUFVeUIsV0FBVixHQUF3Qm1CLFNBQXhCO0FBQ0g7QUFDRGpFLGFBQUtzQixPQUFMLENBQWFDLE9BQWIsQ0FBcUIsVUFBQ0UsV0FBRCxFQUFjNEMsQ0FBZCxFQUFvQjtBQUNyQyxnQkFBSXJFLEtBQUtzQixPQUFMLENBQWErQyxJQUFJLENBQWpCLEtBQXVCckUsS0FBS3NCLE9BQUwsQ0FBYStDLElBQUksQ0FBakIsRUFBb0IxQyxJQUFwQixLQUE2QixPQUF4RCxFQUFpRTtBQUM3RCx1QkFBS04sSUFBTCxDQUFVOEMsSUFBVixHQUFpQixLQUFqQjtBQUNIO0FBQ0QsbUJBQUszQyxPQUFMLENBQWFDLFdBQWIsRUFBMEIwQyxJQUExQjtBQUNILFNBTEQ7QUFNQXpDLGVBQU9VLEtBQVAsQ0FBYUcsSUFBYixDQUFrQjRCLElBQWxCO0FBQ0EsYUFBSzlDLElBQUwsQ0FBVThDLElBQVYsR0FBaUIsS0FBakI7QUFDSCxLOzt5QkFDREcsUyxzQkFBVXRFLEksRUFBTTBCLE0sRUFBUTtBQUNwQkEsZUFBT0ssUUFBUCxVQUF1Qi9CLEtBQUtzQixPQUE1QjtBQUNILEs7O3lCQUNEaUQsUSxxQkFBU3ZFLEksRUFBTTBCLE0sRUFBUTtBQUNuQkEsZUFBT0ssUUFBUCxJQUFtQi9CLEtBQUtzQixPQUF4QjtBQUNILEs7O3lCQUNEa0QsUSxxQkFBU3hFLEksRUFBTTBCLE0sRUFBUTtBQUNuQixZQUFJLEtBQUtMLElBQUwsQ0FBVThDLElBQWQsRUFBb0I7QUFDaEJ6QyxtQkFBT0ssUUFBUCxVQUF3Qi9CLEtBQUtzQixPQUFMLENBQWEsQ0FBYixFQUFnQkEsT0FBeEM7QUFDSCxTQUZELE1BRU87QUFDSEksbUJBQU9LLFFBQVAsVUFBd0IvQixLQUFLc0IsT0FBTCxDQUFhLENBQWIsRUFBZ0JBLE9BQXhDO0FBQ0g7QUFDSixLOzt5QkFDRG1ELEssa0JBQU16RSxJLEVBQU0wQixNLEVBQVE7QUFDaEJBLGVBQU9LLFFBQVAsSUFBbUIvQixLQUFLc0IsT0FBeEI7QUFDSCxLOzs7OztrQkFHVXpCLFUiLCJmaWxlIjoicGFyc2VyLmpzIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHBvc3Rjc3MgZnJvbSAncG9zdGNzcyc7XG5pbXBvcnQgZ29uemFsZXMgZnJvbSAnZ29uemFsZXMtcGUnO1xuXG5jb25zdCBERUZBVUxUX1JBV1NfUk9PVCA9IHtcbiAgICBiZWZvcmU6ICcnXG59O1xuXG5jb25zdCBERUZBVUxUX1JBV1NfUlVMRSA9IHtcbiAgICBiZWZvcmU6ICcnLFxuICAgIGJldHdlZW46ICcnXG59O1xuXG5jb25zdCBERUZBVUxUX1JBV1NfREVDTCA9IHtcbiAgICBiZWZvcmU6ICcnLFxuICAgIGJldHdlZW46ICcnLFxuICAgIHNlbWljb2xvbjogZmFsc2Vcbn07XG5cbmNvbnN0IERFRkFVTFRfQ09NTUVOVF9ERUNMID0ge1xuICAgIGJlZm9yZTogJycsXG4gICAgbGVmdDogJycsXG4gICAgcmlnaHQ6ICcnXG59O1xuXG5jbGFzcyBTYXNzUGFyc2VyIHtcbiAgICBjb25zdHJ1Y3RvcihpbnB1dCkge1xuICAgICAgICB0aGlzLmlucHV0ID0gaW5wdXQ7XG4gICAgfVxuICAgIHBhcnNlKCkge1xuICAgICAgICB0cnkge1xuICAgICAgICAgICAgdGhpcy5ub2RlID0gZ29uemFsZXMucGFyc2UodGhpcy5pbnB1dC5jc3MsIHsgc3ludGF4OiAnc2FzcycgfSk7XG4gICAgICAgIH0gY2F0Y2ggKGVycm9yKSB7XG4gICAgICAgICAgICB0aHJvdyB0aGlzLmlucHV0LmVycm9yKGVycm9yLm1lc3NhZ2UsIGVycm9yLmxpbmUsIDEpO1xuICAgICAgICB9XG4gICAgICAgIHRoaXMubGluZXMgPSB0aGlzLmlucHV0LmNzcy5tYXRjaCgvXi4qKFxccj9cXG58JCkvZ20pO1xuICAgICAgICB0aGlzLnJvb3QgPSB0aGlzLnN0eWxlc2hlZXQodGhpcy5ub2RlKTtcbiAgICB9XG4gICAgZXh0cmFjdFNvdXJjZShzdGFydCwgZW5kKSB7XG4gICAgICAgIGNvbnN0IG5vZGVMaW5lcyA9IHRoaXMubGluZXMuc2xpY2UoXG4gICAgICAgICAgICBzdGFydC5saW5lIC0gMSxcbiAgICAgICAgICAgIGVuZC5saW5lXG4gICAgICAgICk7XG5cbiAgICAgICAgbm9kZUxpbmVzWzBdID0gbm9kZUxpbmVzWzBdLnN1YnN0cmluZyhzdGFydC5jb2x1bW4gLSAxKTtcbiAgICAgICAgY29uc3QgbGFzdCA9IG5vZGVMaW5lcy5sZW5ndGggLSAxO1xuICAgICAgICBub2RlTGluZXNbbGFzdF0gPSBub2RlTGluZXNbbGFzdF0uc3Vic3RyaW5nKDAsIGVuZC5jb2x1bW4pO1xuXG4gICAgICAgIHJldHVybiBub2RlTGluZXMuam9pbignJyk7XG4gICAgfVxuICAgIHN0eWxlc2hlZXQobm9kZSkge1xuICAgICAgICAvLyBDcmVhdGUgYW5kIHNldCBwYXJhbWV0ZXJzIGZvciBSb290IG5vZGVcbiAgICAgICAgY29uc3Qgcm9vdCA9IHBvc3Rjc3Mucm9vdCgpO1xuICAgICAgICByb290LnNvdXJjZSA9IHtcbiAgICAgICAgICAgIHN0YXJ0OiBub2RlLnN0YXJ0LFxuICAgICAgICAgICAgZW5kOiBub2RlLmVuZCxcbiAgICAgICAgICAgIGlucHV0OiB0aGlzLmlucHV0XG4gICAgICAgIH07XG4gICAgICAgIC8vIFJhd3MgZm9yIHJvb3Qgbm9kZVxuICAgICAgICByb290LnJhd3MgPSB7XG4gICAgICAgICAgICBzZW1pY29sb246IERFRkFVTFRfUkFXU19ST09ULnNlbWljb2xvbixcbiAgICAgICAgICAgIGJlZm9yZTogREVGQVVMVF9SQVdTX1JPT1QuYmVmb3JlXG4gICAgICAgIH07XG4gICAgICAgIC8vIFN0b3JlIHNwYWNlcyBiZWZvcmUgcm9vdCAoaWYgZXhpc3QpXG4gICAgICAgIHRoaXMucmF3cyA9IHtcbiAgICAgICAgICAgIGJlZm9yZTogJydcbiAgICAgICAgfTtcbiAgICAgICAgbm9kZS5jb250ZW50LmZvckVhY2goY29udGVudE5vZGUgPT4gdGhpcy5wcm9jZXNzKGNvbnRlbnROb2RlLCByb290KSk7XG4gICAgICAgIHJldHVybiByb290O1xuICAgIH1cbiAgICBwcm9jZXNzKG5vZGUsIHBhcmVudCkge1xuICAgICAgICBpZiAodGhpc1tub2RlLnR5cGVdKSB7XG4gICAgICAgICAgICByZXR1cm4gdGhpc1tub2RlLnR5cGVdKG5vZGUsIHBhcmVudCkgfHwgbnVsbDtcbiAgICAgICAgfSBlbHNlIHtcbiAgICAgICAgICAgIHJldHVybiBudWxsO1xuICAgICAgICB9XG4gICAgfVxuICAgIHJ1bGVzZXQobm9kZSwgcGFyZW50KSB7XG4gICAgICAgIC8vIExvb3AgdG8gZmluZCB0aGUgZGVlcGVzdCBydWxlc2V0IG5vZGVcbiAgICAgICAgdGhpcy5yYXdzLm11bHRpUnVsZVByb3AgPSAnJztcblxuICAgICAgICBub2RlLmNvbnRlbnQuZm9yRWFjaChjb250ZW50Tm9kZSA9PiB7XG4gICAgICAgICAgICBzd2l0Y2ggKGNvbnRlbnROb2RlLnR5cGUpIHtcbiAgICAgICAgICAgICAgICBjYXNlICdibG9jayc6IHtcbiAgICAgICAgICAgICAgICAgICAgLy8gQ3JlYXRlIFJ1bGUgbm9kZVxuICAgICAgICAgICAgICAgICAgICBjb25zdCBydWxlID0gcG9zdGNzcy5ydWxlKCk7XG4gICAgICAgICAgICAgICAgICAgIHJ1bGUuc2VsZWN0b3IgPSAnJztcbiAgICAgICAgICAgICAgICAgICAgLy8gT2JqZWN0IHRvIHN0b3JlIHJhd3MgZm9yIFJ1bGVcbiAgICAgICAgICAgICAgICAgICAgY29uc3QgcnVsZVJhd3MgPSB7XG4gICAgICAgICAgICAgICAgICAgICAgICBiZWZvcmU6IHRoaXMucmF3cy5iZWZvcmUgfHwgREVGQVVMVF9SQVdTX1JVTEUuYmVmb3JlLFxuICAgICAgICAgICAgICAgICAgICAgICAgYmV0d2VlbjogREVGQVVMVF9SQVdTX1JVTEUuYmV0d2VlblxuICAgICAgICAgICAgICAgICAgICB9O1xuXG4gICAgICAgICAgICAgICAgICAgIC8vIFZhcmlhYmxlIHRvIHN0b3JlIHNwYWNlcyBhbmQgc3ltYm9scyBiZWZvcmUgZGVjbGFyYXRpb24gcHJvcGVydHlcbiAgICAgICAgICAgICAgICAgICAgdGhpcy5yYXdzLmJlZm9yZSA9ICcnO1xuICAgICAgICAgICAgICAgICAgICB0aGlzLnJhd3MuY29tbWVudCA9IGZhbHNlO1xuXG4gICAgICAgICAgICAgICAgICAgIC8vIExvb2sgdXAgdGhyb3cgYWxsIG5vZGVzIGluIGN1cnJlbnQgcnVsZXNldCBub2RlXG4gICAgICAgICAgICAgICAgICAgIG5vZGUuY29udGVudFxuICAgICAgICAgICAgICAgICAgICAgICAgLmZpbHRlcigoeyB0eXBlIH0pID0+IHR5cGUgPT09ICdibG9jaycpXG4gICAgICAgICAgICAgICAgICAgICAgICAuZm9yRWFjaChpbm5lckNvbnRlbnROb2RlID0+IHRoaXMucHJvY2Vzcyhpbm5lckNvbnRlbnROb2RlLCBydWxlKSk7XG5cbiAgICAgICAgICAgICAgICAgICAgaWYgKHJ1bGUubm9kZXMubGVuZ3RoKSB7XG4gICAgICAgICAgICAgICAgICAgICAgICAvLyBXcml0ZSBzZWxlY3RvciB0byBSdWxlXG4gICAgICAgICAgICAgICAgICAgICAgICBydWxlLnNlbGVjdG9yID0gdGhpcy5leHRyYWN0U291cmNlKFxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIG5vZGUuc3RhcnQsXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgY29udGVudE5vZGUuc3RhcnRcbiAgICAgICAgICAgICAgICAgICAgICAgICkuc2xpY2UoMCwgLTEpLnJlcGxhY2UoL1xccyskLywgc3BhY2VzID0+IHtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBydWxlUmF3cy5iZXR3ZWVuID0gc3BhY2VzO1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgIHJldHVybiAnJztcbiAgICAgICAgICAgICAgICAgICAgICAgIH0pO1xuICAgICAgICAgICAgICAgICAgICAgICAgLy8gU2V0IHBhcmFtZXRlcnMgZm9yIFJ1bGUgbm9kZVxuICAgICAgICAgICAgICAgICAgICAgICAgcnVsZS5wYXJlbnQgPSBwYXJlbnQ7XG4gICAgICAgICAgICAgICAgICAgICAgICBydWxlLnNvdXJjZSA9IHtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBzdGFydDogbm9kZS5zdGFydCxcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBlbmQ6IG5vZGUuZW5kLFxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIGlucHV0OiB0aGlzLmlucHV0XG4gICAgICAgICAgICAgICAgICAgICAgICB9O1xuICAgICAgICAgICAgICAgICAgICAgICAgcnVsZS5yYXdzID0gcnVsZVJhd3M7XG4gICAgICAgICAgICAgICAgICAgICAgICBwYXJlbnQubm9kZXMucHVzaChydWxlKTtcbiAgICAgICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgICAgICAgICBicmVhaztcbiAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICAgICAgZGVmYXVsdDpcbiAgICAgICAgICAgIH1cbiAgICAgICAgfSk7XG4gICAgfVxuICAgIGJsb2NrKG5vZGUsIHBhcmVudCkge1xuICAgICAgICAvLyBJZiBuZXN0ZWQgcnVsZXMgZXhpc3QsIHdyYXAgY3VycmVudCBydWxlIGluIG5ldyBydWxlIG5vZGVcbiAgICAgICAgaWYgKHRoaXMucmF3cy5tdWx0aVJ1bGUpIHtcbiAgICAgICAgICAgIGlmICh0aGlzLnJhd3MubXVsdGlSdWxlUHJvcFZhcmlhYmxlKSB7XG4gICAgICAgICAgICAgICAgdGhpcy5yYXdzLm11bHRpUnVsZVByb3AgPSBgXFwkJHt0aGlzLnJhd3MubXVsdGlSdWxlUHJvcH1gO1xuICAgICAgICAgICAgfVxuICAgICAgICAgICAgY29uc3QgbXVsdGlSdWxlID0gT2JqZWN0LmFzc2lnbihwb3N0Y3NzLnJ1bGUoKSwge1xuICAgICAgICAgICAgICAgIHNvdXJjZToge1xuICAgICAgICAgICAgICAgICAgICBzdGFydDoge1xuICAgICAgICAgICAgICAgICAgICAgICAgbGluZTogbm9kZS5zdGFydC5saW5lIC0gMSxcbiAgICAgICAgICAgICAgICAgICAgICAgIGNvbHVtbjogbm9kZS5zdGFydC5jb2x1bW5cbiAgICAgICAgICAgICAgICAgICAgfSxcbiAgICAgICAgICAgICAgICAgICAgZW5kOiBub2RlLmVuZCxcbiAgICAgICAgICAgICAgICAgICAgaW5wdXQ6IHRoaXMuaW5wdXRcbiAgICAgICAgICAgICAgICB9LFxuICAgICAgICAgICAgICAgIHJhd3M6IHtcbiAgICAgICAgICAgICAgICAgICAgYmVmb3JlOiB0aGlzLnJhd3MuYmVmb3JlIHx8IERFRkFVTFRfUkFXU19SVUxFLmJlZm9yZSxcbiAgICAgICAgICAgICAgICAgICAgYmV0d2VlbjogREVGQVVMVF9SQVdTX1JVTEUuYmV0d2VlblxuICAgICAgICAgICAgICAgIH0sXG4gICAgICAgICAgICAgICAgcGFyZW50LFxuICAgICAgICAgICAgICAgIHNlbGVjdG9yOiAodGhpcy5yYXdzLmN1c3RvbVByb3BlcnR5ID8gJy0tJyA6ICcnKSArIHRoaXMucmF3cy5tdWx0aVJ1bGVQcm9wXG4gICAgICAgICAgICB9KTtcbiAgICAgICAgICAgIHBhcmVudC5wdXNoKG11bHRpUnVsZSk7XG4gICAgICAgICAgICBwYXJlbnQgPSBtdWx0aVJ1bGU7XG4gICAgICAgIH1cblxuICAgICAgICB0aGlzLnJhd3MuYmVmb3JlID0gJyc7XG5cbiAgICAgICAgLy8gTG9va2luZyBmb3IgZGVjbGFyYXRpb24gbm9kZSBpbiBibG9jayBub2RlXG4gICAgICAgIG5vZGUuY29udGVudC5mb3JFYWNoKGNvbnRlbnROb2RlID0+IHRoaXMucHJvY2Vzcyhjb250ZW50Tm9kZSwgcGFyZW50KSk7XG4gICAgICAgIGlmICh0aGlzLnJhd3MubXVsdGlSdWxlKSB7XG4gICAgICAgICAgICB0aGlzLnJhd3MuYmVmb3JlTXVsdGkgPSB0aGlzLnJhd3MuYmVmb3JlO1xuICAgICAgICB9XG4gICAgfVxuICAgIGRlY2xhcmF0aW9uKG5vZGUsIHBhcmVudCkge1xuICAgICAgICBsZXQgaXNCbG9ja0luc2lkZSA9IGZhbHNlO1xuICAgICAgICAvLyBDcmVhdGUgRGVjbGFyYXRpb24gbm9kZVxuICAgICAgICBjb25zdCBkZWNsYXJhdGlvbk5vZGUgPSBwb3N0Y3NzLmRlY2woKTtcbiAgICAgICAgZGVjbGFyYXRpb25Ob2RlLnByb3AgPSAnJztcblxuICAgICAgICAvLyBPYmplY3QgdG8gc3RvcmUgcmF3cyBmb3IgRGVjbGFyYXRpb25cbiAgICAgICAgY29uc3QgZGVjbGFyYXRpb25SYXdzID0gT2JqZWN0LmFzc2lnbihkZWNsYXJhdGlvbk5vZGUucmF3cywge1xuICAgICAgICAgICAgYmVmb3JlOiB0aGlzLnJhd3MuYmVmb3JlIHx8IERFRkFVTFRfUkFXU19ERUNMLmJlZm9yZSxcbiAgICAgICAgICAgIGJldHdlZW46IERFRkFVTFRfUkFXU19ERUNMLmJldHdlZW4sXG4gICAgICAgICAgICBzZW1pY29sb246IERFRkFVTFRfUkFXU19ERUNMLnNlbWljb2xvblxuICAgICAgICB9KTtcblxuICAgICAgICB0aGlzLnJhd3MucHJvcGVydHkgPSBmYWxzZTtcbiAgICAgICAgdGhpcy5yYXdzLmJldHdlZW5CZWZvcmUgPSBmYWxzZTtcbiAgICAgICAgdGhpcy5yYXdzLmNvbW1lbnQgPSBmYWxzZTtcbiAgICAgICAgLy8gTG9va2luZyBmb3IgcHJvcGVydHkgYW5kIHZhbHVlIG5vZGUgaW4gZGVjbGFyYXRpb24gbm9kZVxuICAgICAgICBub2RlLmNvbnRlbnQuZm9yRWFjaCgoY29udGVudE5vZGUpID0+IHtcbiAgICAgICAgICAgIHN3aXRjaCAoY29udGVudE5vZGUudHlwZSkge1xuICAgICAgICAgICAgICAgIGNhc2UgJ2N1c3RvbVByb3BlcnR5JzpcbiAgICAgICAgICAgICAgICAgICAgdGhpcy5yYXdzLmN1c3RvbVByb3BlcnR5ID0gdHJ1ZTtcbiAgICAgICAgICAgICAgICAgICAgLy8gZmFsbCB0aHJvdWdoXG4gICAgICAgICAgICAgICAgY2FzZSAncHJvcGVydHknOiB7XG4gICAgICAgICAgICAgICAgICAgIC8qIGdsb2JhbC5wcm9wZXJ0eSB0byBkZXRlY3QgaXMgcHJvcGVydHkgaXMgYWxyZWFkeSBkZWZpbmVkIGluIGN1cnJlbnQgb2JqZWN0ICovXG4gICAgICAgICAgICAgICAgICAgIHRoaXMucmF3cy5wcm9wZXJ0eSA9IHRydWU7XG4gICAgICAgICAgICAgICAgICAgIHRoaXMucmF3cy5tdWx0aVJ1bGVQcm9wID0gY29udGVudE5vZGUuY29udGVudFswXS5jb250ZW50O1xuICAgICAgICAgICAgICAgICAgICB0aGlzLnJhd3MubXVsdGlSdWxlUHJvcFZhcmlhYmxlID0gY29udGVudE5vZGUuY29udGVudFswXS50eXBlID09PSAndmFyaWFibGUnO1xuICAgICAgICAgICAgICAgICAgICB0aGlzLnByb2Nlc3MoY29udGVudE5vZGUsIGRlY2xhcmF0aW9uTm9kZSk7XG4gICAgICAgICAgICAgICAgICAgIGJyZWFrO1xuICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgICAgICBjYXNlICdwcm9wZXJ0eURlbGltaXRlcic6IHtcbiAgICAgICAgICAgICAgICAgICAgaWYgKHRoaXMucmF3cy5wcm9wZXJ0eSAmJiAhdGhpcy5yYXdzLmJldHdlZW5CZWZvcmUpIHtcbiAgICAgICAgICAgICAgICAgICAgICAgIC8qIElmIHByb3BlcnR5IGlzIGFscmVhZHkgZGVmaW5lZCBhbmQgdGhlcmUncyBubyAnOicgYmVmb3JlIGl0ICovXG4gICAgICAgICAgICAgICAgICAgICAgICBkZWNsYXJhdGlvblJhd3MuYmV0d2VlbiArPSBjb250ZW50Tm9kZS5jb250ZW50O1xuICAgICAgICAgICAgICAgICAgICAgICAgdGhpcy5yYXdzLm11bHRpUnVsZVByb3AgKz0gY29udGVudE5vZGUuY29udGVudDtcbiAgICAgICAgICAgICAgICAgICAgfSBlbHNlIHtcbiAgICAgICAgICAgICAgICAgICAgICAgIC8qIElmICc6JyBnb2VzIGJlZm9yZSBwcm9wZXJ0eSBkZWNsYXJhdGlvbiwgbGlrZSA6d2lkdGggMTAwcHggKi9cbiAgICAgICAgICAgICAgICAgICAgICAgIHRoaXMucmF3cy5iZXR3ZWVuQmVmb3JlID0gdHJ1ZTtcbiAgICAgICAgICAgICAgICAgICAgICAgIGRlY2xhcmF0aW9uUmF3cy5iZWZvcmUgKz0gY29udGVudE5vZGUuY29udGVudDtcbiAgICAgICAgICAgICAgICAgICAgICAgIHRoaXMucmF3cy5tdWx0aVJ1bGVQcm9wICs9IGNvbnRlbnROb2RlLmNvbnRlbnQ7XG4gICAgICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgICAgICAgICAgYnJlYWs7XG4gICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgICAgIGNhc2UgJ3NwYWNlJzoge1xuICAgICAgICAgICAgICAgICAgICBkZWNsYXJhdGlvblJhd3MuYmV0d2VlbiArPSBjb250ZW50Tm9kZS5jb250ZW50O1xuICAgICAgICAgICAgICAgICAgICBicmVhaztcbiAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICAgICAgY2FzZSAndmFsdWUnOiB7XG4gICAgICAgICAgICAgICAgICAgIC8vIExvb2sgdXAgZm9yIGEgdmFsdWUgZm9yIGN1cnJlbnQgcHJvcGVydHlcbiAgICAgICAgICAgICAgICAgICAgc3dpdGNoIChjb250ZW50Tm9kZS5jb250ZW50WzBdLnR5cGUpIHtcbiAgICAgICAgICAgICAgICAgICAgICAgIGNhc2UgJ2Jsb2NrJzoge1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgIGlzQmxvY2tJbnNpZGUgPSB0cnVlO1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgIC8vIElmIG5lc3RlZCBydWxlcyBleGlzdFxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIGlmIChBcnJheS5pc0FycmF5KGNvbnRlbnROb2RlLmNvbnRlbnRbMF0uY29udGVudCkpIHtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgdGhpcy5yYXdzLm11bHRpUnVsZSA9IHRydWU7XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIHRoaXMucHJvY2Vzcyhjb250ZW50Tm9kZS5jb250ZW50WzBdLCBwYXJlbnQpO1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgIGJyZWFrO1xuICAgICAgICAgICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgICAgICAgICAgICAgY2FzZSAndmFyaWFibGUnOiB7XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgZGVjbGFyYXRpb25Ob2RlLnZhbHVlID0gJyQnO1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgIHRoaXMucHJvY2Vzcyhjb250ZW50Tm9kZSwgZGVjbGFyYXRpb25Ob2RlKTtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBicmVhaztcbiAgICAgICAgICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgICAgICAgICAgICAgIGNhc2UgJ2NvbG9yJzoge1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgIGRlY2xhcmF0aW9uTm9kZS52YWx1ZSA9ICcjJztcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICB0aGlzLnByb2Nlc3MoY29udGVudE5vZGUsIGRlY2xhcmF0aW9uTm9kZSk7XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgYnJlYWs7XG4gICAgICAgICAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICAgICAgICAgICAgICBjYXNlICdudW1iZXInOiB7XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgaWYgKGNvbnRlbnROb2RlLmNvbnRlbnQubGVuZ3RoID4gMSkge1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBkZWNsYXJhdGlvbk5vZGUudmFsdWUgPSBjb250ZW50Tm9kZS5jb250ZW50LmpvaW4oJycpO1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgIH0gZWxzZSB7XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIHRoaXMucHJvY2Vzcyhjb250ZW50Tm9kZSwgZGVjbGFyYXRpb25Ob2RlKTtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgYnJlYWs7XG4gICAgICAgICAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICAgICAgICAgICAgICBjYXNlICdwYXJlbnRoZXNlcyc6IHtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBkZWNsYXJhdGlvbk5vZGUudmFsdWUgPSAnKCc7XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgdGhpcy5wcm9jZXNzKGNvbnRlbnROb2RlLCBkZWNsYXJhdGlvbk5vZGUpO1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgIGJyZWFrO1xuICAgICAgICAgICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgICAgICAgICAgICAgZGVmYXVsdDoge1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgIHRoaXMucHJvY2Vzcyhjb250ZW50Tm9kZSwgZGVjbGFyYXRpb25Ob2RlKTtcbiAgICAgICAgICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgICAgICAgICBicmVhaztcbiAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICAgICAgZGVmYXVsdDpcbiAgICAgICAgICAgIH1cbiAgICAgICAgfSk7XG5cbiAgICAgICAgaWYgKCFpc0Jsb2NrSW5zaWRlKSB7XG4gICAgICAgICAgICAvLyBTZXQgcGFyYW1ldGVycyBmb3IgRGVjbGFyYXRpb24gbm9kZVxuICAgICAgICAgICAgZGVjbGFyYXRpb25Ob2RlLnNvdXJjZSA9IHtcbiAgICAgICAgICAgICAgICBzdGFydDogbm9kZS5zdGFydCxcbiAgICAgICAgICAgICAgICBlbmQ6IG5vZGUuZW5kLFxuICAgICAgICAgICAgICAgIGlucHV0OiB0aGlzLmlucHV0XG4gICAgICAgICAgICB9O1xuICAgICAgICAgICAgZGVjbGFyYXRpb25Ob2RlLnBhcmVudCA9IHBhcmVudDtcbiAgICAgICAgICAgIHBhcmVudC5ub2Rlcy5wdXNoKGRlY2xhcmF0aW9uTm9kZSk7XG4gICAgICAgIH1cblxuICAgICAgICB0aGlzLnJhd3MuYmVmb3JlID0gJyc7XG4gICAgICAgIHRoaXMucmF3cy5jdXN0b21Qcm9wZXJ0eSA9IGZhbHNlO1xuICAgICAgICB0aGlzLnJhd3MubXVsdGlSdWxlUHJvcCA9ICcnO1xuICAgICAgICB0aGlzLnJhd3MucHJvcGVydHkgPSBmYWxzZTtcbiAgICB9XG4gICAgY3VzdG9tUHJvcGVydHkobm9kZSwgcGFyZW50KSB7XG4gICAgICAgIHRoaXMucHJvcGVydHkobm9kZSwgcGFyZW50KTtcbiAgICAgICAgcGFyZW50LnByb3AgPSBgLS0ke3BhcmVudC5wcm9wfWA7XG4gICAgfVxuICAgIHByb3BlcnR5KG5vZGUsIHBhcmVudCkge1xuICAgICAgICAvLyBTZXQgcHJvcGVydHkgZm9yIERlY2xhcmF0aW9uIG5vZGVcbiAgICAgICAgc3dpdGNoIChub2RlLmNvbnRlbnRbMF0udHlwZSkge1xuICAgICAgICAgICAgY2FzZSAndmFyaWFibGUnOiB7XG4gICAgICAgICAgICAgICAgcGFyZW50LnByb3AgKz0gJyQnO1xuICAgICAgICAgICAgICAgIGJyZWFrO1xuICAgICAgICAgICAgfVxuICAgICAgICAgICAgY2FzZSAnaW50ZXJwb2xhdGlvbic6IHtcbiAgICAgICAgICAgICAgICB0aGlzLnJhd3MuaW50ZXJwb2xhdGlvbiA9IHRydWU7XG4gICAgICAgICAgICAgICAgcGFyZW50LnByb3AgKz0gJyN7JztcbiAgICAgICAgICAgICAgICBicmVhaztcbiAgICAgICAgICAgIH1cbiAgICAgICAgICAgIGRlZmF1bHQ6XG4gICAgICAgIH1cbiAgICAgICAgcGFyZW50LnByb3AgKz0gbm9kZS5jb250ZW50WzBdLmNvbnRlbnQ7XG4gICAgICAgIGlmICh0aGlzLnJhd3MuaW50ZXJwb2xhdGlvbikge1xuICAgICAgICAgICAgcGFyZW50LnByb3AgKz0gJ30nO1xuICAgICAgICAgICAgdGhpcy5yYXdzLmludGVycG9sYXRpb24gPSBmYWxzZTtcbiAgICAgICAgfVxuICAgIH1cbiAgICB2YWx1ZShub2RlLCBwYXJlbnQpIHtcbiAgICAgICAgaWYgKCFwYXJlbnQudmFsdWUpIHtcbiAgICAgICAgICAgIHBhcmVudC52YWx1ZSA9ICcnO1xuICAgICAgICB9XG4gICAgICAgIC8vIFNldCB2YWx1ZSBmb3IgRGVjbGFyYXRpb24gbm9kZVxuICAgICAgICBpZiAobm9kZS5jb250ZW50Lmxlbmd0aCkge1xuICAgICAgICAgICAgbm9kZS5jb250ZW50LmZvckVhY2goY29udGVudE5vZGUgPT4ge1xuICAgICAgICAgICAgICAgIHN3aXRjaCAoY29udGVudE5vZGUudHlwZSkge1xuICAgICAgICAgICAgICAgICAgICBjYXNlICdpbXBvcnRhbnQnOiB7XG4gICAgICAgICAgICAgICAgICAgICAgICBwYXJlbnQucmF3cy5pbXBvcnRhbnQgPSBjb250ZW50Tm9kZS5jb250ZW50O1xuICAgICAgICAgICAgICAgICAgICAgICAgcGFyZW50LmltcG9ydGFudCA9IHRydWU7XG4gICAgICAgICAgICAgICAgICAgICAgICBjb25zdCBtYXRjaCA9IHBhcmVudC52YWx1ZS5tYXRjaCgvXiguKj8pKFxccyopJC8pO1xuICAgICAgICAgICAgICAgICAgICAgICAgaWYgKG1hdGNoKSB7XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgcGFyZW50LnJhd3MuaW1wb3J0YW50ID0gbWF0Y2hbMl0gKyBwYXJlbnQucmF3cy5pbXBvcnRhbnQ7XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgcGFyZW50LnZhbHVlID0gbWF0Y2hbMV07XG4gICAgICAgICAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICAgICAgICAgICAgICBicmVhaztcbiAgICAgICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgICAgICAgICBjYXNlICdwYXJlbnRoZXNlcyc6IHtcbiAgICAgICAgICAgICAgICAgICAgICAgIHBhcmVudC52YWx1ZSArPSBjb250ZW50Tm9kZS5jb250ZW50LmpvaW4oJycpICsgJyknO1xuICAgICAgICAgICAgICAgICAgICAgICAgYnJlYWs7XG4gICAgICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgICAgICAgICAgY2FzZSAncGVyY2VudGFnZSc6IHtcbiAgICAgICAgICAgICAgICAgICAgICAgIHBhcmVudC52YWx1ZSArPSBjb250ZW50Tm9kZS5jb250ZW50LmpvaW4oJycpICsgJyUnO1xuICAgICAgICAgICAgICAgICAgICAgICAgYnJlYWs7XG4gICAgICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgICAgICAgICAgZGVmYXVsdDoge1xuICAgICAgICAgICAgICAgICAgICAgICAgaWYgKGNvbnRlbnROb2RlLmNvbnRlbnQuY29uc3RydWN0b3IgPT09IEFycmF5KSB7XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgcGFyZW50LnZhbHVlICs9IGNvbnRlbnROb2RlLmNvbnRlbnQuam9pbignJyk7XG4gICAgICAgICAgICAgICAgICAgICAgICB9IGVsc2Uge1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgIHBhcmVudC52YWx1ZSArPSBjb250ZW50Tm9kZS5jb250ZW50O1xuICAgICAgICAgICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgfSk7XG4gICAgICAgIH1cbiAgICB9XG4gICAgc2luZ2xlbGluZUNvbW1lbnQobm9kZSwgcGFyZW50KSB7XG4gICAgICAgIHJldHVybiB0aGlzLmNvbW1lbnQobm9kZSwgcGFyZW50LCB0cnVlKTtcbiAgICB9XG4gICAgbXVsdGlsaW5lQ29tbWVudChub2RlLCBwYXJlbnQpIHtcbiAgICAgICAgcmV0dXJuIHRoaXMuY29tbWVudChub2RlLCBwYXJlbnQsIGZhbHNlKTtcbiAgICB9XG4gICAgY29tbWVudChub2RlLCBwYXJlbnQsIGlubGluZSkge1xuICAgICAgICBjb25zdCB0ZXh0ID0gbm9kZS5jb250ZW50Lm1hdGNoKC9eKFxccyopKCg/OlxcU1tcXHNcXFNdKj8pPykoXFxzKikkLyk7XG5cbiAgICAgICAgdGhpcy5yYXdzLmNvbW1lbnQgPSB0cnVlO1xuXG4gICAgICAgIGNvbnN0IGNvbW1lbnQgPSBPYmplY3QuYXNzaWduKHBvc3Rjc3MuY29tbWVudCgpLCB7XG4gICAgICAgICAgICB0ZXh0OiB0ZXh0WzJdLFxuICAgICAgICAgICAgcmF3czoge1xuICAgICAgICAgICAgICAgIGJlZm9yZTogdGhpcy5yYXdzLmJlZm9yZSB8fCBERUZBVUxUX0NPTU1FTlRfREVDTC5iZWZvcmUsXG4gICAgICAgICAgICAgICAgbGVmdDogdGV4dFsxXSxcbiAgICAgICAgICAgICAgICByaWdodDogdGV4dFszXSxcbiAgICAgICAgICAgICAgICBpbmxpbmVcbiAgICAgICAgICAgIH1cbiAgICAgICAgfSk7XG5cbiAgICAgICAgaWYgKHRoaXMucmF3cy5iZWZvcmVNdWx0aSkge1xuICAgICAgICAgICAgY29tbWVudC5yYXdzLmJlZm9yZSArPSB0aGlzLnJhd3MuYmVmb3JlTXVsdGk7XG4gICAgICAgICAgICB0aGlzLnJhd3MuYmVmb3JlTXVsdGkgPSB1bmRlZmluZWQ7XG4gICAgICAgIH1cblxuICAgICAgICBwYXJlbnQubm9kZXMucHVzaChjb21tZW50KTtcbiAgICAgICAgdGhpcy5yYXdzLmJlZm9yZSA9ICcnO1xuICAgIH1cbiAgICBzcGFjZShub2RlLCBwYXJlbnQpIHtcbiAgICAgICAgLy8gU3BhY2VzIGJlZm9yZSByb290IGFuZCBydWxlXG4gICAgICAgIHN3aXRjaCAocGFyZW50LnR5cGUpIHtcbiAgICAgICAgICAgIGNhc2UgJ3Jvb3QnOiB7XG4gICAgICAgICAgICAgICAgdGhpcy5yYXdzLmJlZm9yZSArPSBub2RlLmNvbnRlbnQ7XG4gICAgICAgICAgICAgICAgYnJlYWs7XG4gICAgICAgICAgICB9XG4gICAgICAgICAgICBjYXNlICdydWxlJzoge1xuICAgICAgICAgICAgICAgIGlmICh0aGlzLnJhd3MuY29tbWVudCkge1xuICAgICAgICAgICAgICAgICAgICB0aGlzLnJhd3MuYmVmb3JlICs9IG5vZGUuY29udGVudDtcbiAgICAgICAgICAgICAgICB9IGVsc2UgaWYgKHRoaXMucmF3cy5sb29wKSB7XG4gICAgICAgICAgICAgICAgICAgIHBhcmVudC5zZWxlY3RvciArPSBub2RlLmNvbnRlbnQ7XG4gICAgICAgICAgICAgICAgfSBlbHNlIHtcbiAgICAgICAgICAgICAgICAgICAgdGhpcy5yYXdzLmJlZm9yZSA9ICh0aGlzLnJhd3MuYmVmb3JlIHx8ICdcXG4nKSArIG5vZGUuY29udGVudDtcbiAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICAgICAgYnJlYWs7XG4gICAgICAgICAgICB9XG4gICAgICAgICAgICBkZWZhdWx0OlxuICAgICAgICB9XG4gICAgfVxuICAgIGRlY2xhcmF0aW9uRGVsaW1pdGVyKG5vZGUpIHtcbiAgICAgICAgdGhpcy5yYXdzLmJlZm9yZSArPSBub2RlLmNvbnRlbnQ7XG4gICAgfVxuICAgIGxvb3Aobm9kZSwgcGFyZW50KSB7XG4gICAgICAgIGNvbnN0IGxvb3AgPSBwb3N0Y3NzLnJ1bGUoKTtcbiAgICAgICAgdGhpcy5yYXdzLmNvbW1lbnQgPSBmYWxzZTtcbiAgICAgICAgdGhpcy5yYXdzLm11bHRpUnVsZSA9IGZhbHNlO1xuICAgICAgICB0aGlzLnJhd3MubG9vcCA9IHRydWU7XG4gICAgICAgIGxvb3Auc2VsZWN0b3IgPSAnJztcbiAgICAgICAgbG9vcC5yYXdzID0ge1xuICAgICAgICAgICAgYmVmb3JlOiB0aGlzLnJhd3MuYmVmb3JlIHx8IERFRkFVTFRfUkFXU19SVUxFLmJlZm9yZSxcbiAgICAgICAgICAgIGJldHdlZW46IERFRkFVTFRfUkFXU19SVUxFLmJldHdlZW5cbiAgICAgICAgfTtcbiAgICAgICAgaWYgKHRoaXMucmF3cy5iZWZvcmVNdWx0aSkge1xuICAgICAgICAgICAgbG9vcC5yYXdzLmJlZm9yZSArPSB0aGlzLnJhd3MuYmVmb3JlTXVsdGk7XG4gICAgICAgICAgICB0aGlzLnJhd3MuYmVmb3JlTXVsdGkgPSB1bmRlZmluZWQ7XG4gICAgICAgIH1cbiAgICAgICAgbm9kZS5jb250ZW50LmZvckVhY2goKGNvbnRlbnROb2RlLCBpKSA9PiB7XG4gICAgICAgICAgICBpZiAobm9kZS5jb250ZW50W2kgKyAxXSAmJiBub2RlLmNvbnRlbnRbaSArIDFdLnR5cGUgPT09ICdibG9jaycpIHtcbiAgICAgICAgICAgICAgICB0aGlzLnJhd3MubG9vcCA9IGZhbHNlO1xuICAgICAgICAgICAgfVxuICAgICAgICAgICAgdGhpcy5wcm9jZXNzKGNvbnRlbnROb2RlLCBsb29wKTtcbiAgICAgICAgfSk7XG4gICAgICAgIHBhcmVudC5ub2Rlcy5wdXNoKGxvb3ApO1xuICAgICAgICB0aGlzLnJhd3MubG9vcCA9IGZhbHNlO1xuICAgIH1cbiAgICBhdGtleXdvcmQobm9kZSwgcGFyZW50KSB7XG4gICAgICAgIHBhcmVudC5zZWxlY3RvciArPSBgQCR7bm9kZS5jb250ZW50fWA7XG4gICAgfVxuICAgIG9wZXJhdG9yKG5vZGUsIHBhcmVudCkge1xuICAgICAgICBwYXJlbnQuc2VsZWN0b3IgKz0gbm9kZS5jb250ZW50O1xuICAgIH1cbiAgICB2YXJpYWJsZShub2RlLCBwYXJlbnQpIHtcbiAgICAgICAgaWYgKHRoaXMucmF3cy5sb29wKSB7XG4gICAgICAgICAgICBwYXJlbnQuc2VsZWN0b3IgKz0gYFxcJCR7bm9kZS5jb250ZW50WzBdLmNvbnRlbnR9YDtcbiAgICAgICAgfSBlbHNlIHtcbiAgICAgICAgICAgIHBhcmVudC5zZWxlY3RvciArPSBgXFwjJHtub2RlLmNvbnRlbnRbMF0uY29udGVudH1gO1xuICAgICAgICB9XG4gICAgfVxuICAgIGlkZW50KG5vZGUsIHBhcmVudCkge1xuICAgICAgICBwYXJlbnQuc2VsZWN0b3IgKz0gbm9kZS5jb250ZW50O1xuICAgIH1cbn1cblxuZXhwb3J0IGRlZmF1bHQgU2Fzc1BhcnNlcjtcbiJdfQ==