"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
const ELEMENT_NODE = exports.ELEMENT_NODE = 1;
const TEXT_NODE = exports.TEXT_NODE = 3;
const COMMENT_NODE = exports.COMMENT_NODE = 8;
const FRAGMENT_NODE = exports.FRAGMENT_NODE = 11;