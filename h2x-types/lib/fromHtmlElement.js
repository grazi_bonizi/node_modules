'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _symbols = require('./symbols');

var _fromHtmlAttribute = require('./fromHtmlAttribute');

var _fromHtmlAttribute2 = _interopRequireDefault(_fromHtmlAttribute);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

const HTML_ELEMENT_PROPERTIES = ['tagName', 'ownerDocument', 'textContent'];

class HTMLElementNode {

  constructor(originalNode) {
    this.originalNode = originalNode;
    this.childNodes = originalNode.childNodes ? Array.from(originalNode.childNodes).map(childNode => fromHtmlElement(childNode)) : null;
    this.attributes = originalNode.attributes ? Array.from(originalNode.attributes).map(attribute => (0, _fromHtmlAttribute2.default)(attribute)) : null;
    HTML_ELEMENT_PROPERTIES.forEach(property => {
      this[property] = originalNode[property];
    });
  }
}

HTMLElementNode[_symbols.VISITOR_KEYS] = ['childNodes', 'attributes'];
function fromHtmlElement(htmlNode) {
  return new HTMLElementNode(htmlNode);
}

exports.default = fromHtmlElement;