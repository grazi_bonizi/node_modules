'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _symbols = require('./symbols');

const HTML_ELEMENT_PROPERTIES = ['name', 'value'];

class HTMLAttributeNode {

  constructor(originalNode) {
    this.originalNode = originalNode;
    HTML_ELEMENT_PROPERTIES.forEach(property => {
      this[property] = originalNode[property];
    });
  }
}

HTMLAttributeNode[_symbols.VISITOR_KEYS] = null;
function fromHtmlAttribute(attribute) {
  return new HTMLAttributeNode(attribute);
}

exports.default = fromHtmlAttribute;