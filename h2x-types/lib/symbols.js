'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});
const NODE_TYPE = exports.NODE_TYPE = Symbol.for('NODE_TYPE');
const VISITOR_KEYS = exports.VISITOR_KEYS = Symbol.for('VISITOR_KEYS');