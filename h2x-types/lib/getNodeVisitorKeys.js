'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _symbols = require('./symbols');

function getNodeVisitorKeys(node) {
  return node.constructor[_symbols.VISITOR_KEYS] || null;
}

exports.default = getNodeVisitorKeys;