'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _symbols = require('./symbols');

var _fromHtmlAttribute = require('./fromHtmlAttribute');

var _fromHtmlAttribute2 = _interopRequireDefault(_fromHtmlAttribute);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

const HTML_NODE_PROPERTIES = ['tagName', 'ownerDocument', 'textContent'];

class HTMLNode {

  constructor(originalNode) {
    this.childNodes = originalNode.childNodes ? Array.from(originalNode.childNodes).map(childNode => fromHtmlNode(childNode)) : null;
    this.attributes = originalNode.attributes ? Array.from(originalNode.attributes).map(attribute => (0, _fromHtmlAttribute2.default)(attribute)) : null;
    HTML_NODE_PROPERTIES.forEach(property => {
      this[property] = originalNode[property];
    });
  }
}

HTMLNode[_symbols.VISITOR_KEYS] = ['childNodes', 'attributes'];
function fromHtmlNode(htmlNode) {
  return new HTMLNodeWrapper(htmlNode);
}

exports.default = fromHtmlNode;