// third party imports
import { createStore, combineReducers } from 'redux';
import { combineReducers as immutableCombine } from 'redux-immutable';
import { Record } from 'immutable';
// local imports
import getBreakpoints from './getBreakpoints';
import createReducer, { defaultBreakpoints } from './createReducer';

var reducer = createReducer();

describe('Breakpoint discovery', function () {
    it('Can find reducer at root', function () {
        // create a redux store with the reducer at the root
        var store = createStore(combineReducers({
            browser: reducer
        }));

        // make sure we could retrieve the default breakpoints from the store
        expect(getBreakpoints(store)).toBe(defaultBreakpoints);
    });

    it('Can find responsive state in Immutable.js Map root state', function () {
        // create a redux store with the reducer at the root
        var store = createStore(immutableCombine({
            browser: reducer
        }));

        // make sure we could retrieve the default breakpoints from the store
        expect(getBreakpoints(store)).toBe(defaultBreakpoints);
    });

    it('Can find responsive state in Immutable.js Record root state', function () {
        var StateRecord = Record({
            browser: undefined
        });
        // create a redux store with the reducer at the root
        var store = createStore(immutableCombine({
            browser: reducer
        }, StateRecord));

        // make sure we could retrieve the default breakpoints from the store
        expect(getBreakpoints(store)).toBe(defaultBreakpoints);
    });

    it('Can find responsive state anywhere in the state', function () {
        var store = createStore(combineReducers({
            foo: combineReducers({
                bar: combineReducers({
                    baz: reducer
                }),
                quux: function quux() {
                    return true;
                }
            })
        }));
        expect(getBreakpoints(store)).toBe(defaultBreakpoints);
    });

    it('Can find responsive state anywhere in the ImmutableJS Map', function () {
        var store = createStore(immutableCombine({
            foo: immutableCombine({
                bar: immutableCombine({
                    baz: reducer
                }),
                quux: function quux() {
                    return true;
                }
            })
        }));
        expect(getBreakpoints(store)).toBe(defaultBreakpoints);
    });

    it('Can find responsive state anywhere in the ImmutableJS Record', function () {
        var StateRecord = Record({
            foo: Record({
                bar: Record({
                    baz: undefined
                })(),
                quux: true
            })()
        });
        var store = createStore(immutableCombine({
            foo: immutableCombine({
                bar: immutableCombine({
                    baz: reducer
                }),
                quux: function quux() {
                    return true;
                }
            })
        }, StateRecord));
        expect(getBreakpoints(store)).toBe(defaultBreakpoints);
    });

    it('Complains if it cannot find a reducer at root', function () {
        // create a store without the reducer at reducer
        var store = createStore(combineReducers({
            hello: function hello() {
                return true;
            }
        }));

        // make sure this throws an error
        expect(function () {
            return getBreakpoints(store);
        }).toThrowError(Error);
    });

    it('Does not break if state contains \'keys\' or \'getIn\' key somewhere', function () {
        var store = createStore(combineReducers({
            keys: function keys() {
                return true;
            },
            getIn: function getIn() {
                return true;
            },
            browser: reducer
        }));
        expect(function () {
            return getBreakpoints(store);
        }).not.toThrowError();
    });
});