'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _h2xTypes = require('h2x-types');

class JSXComment {
  constructor() {
    this.text = null;
  }

}

JSXComment[_h2xTypes.NODE_TYPE] = 'JSXComment';
JSXComment[_h2xTypes.VISITOR_KEYS] = null;
exports.default = JSXComment;