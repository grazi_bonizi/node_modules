'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _h2xTypes = require('h2x-types');

class JSXText {
  constructor() {
    this.text = null;
  }

}

JSXText[_h2xTypes.NODE_TYPE] = 'JSXText';
JSXText[_h2xTypes.VISITOR_KEYS] = null;
exports.default = JSXText;