'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.JSXText = exports.JSXComment = exports.JSXAttribute = exports.JSXElement = undefined;

var _JSXElement = require('./JSXElement');

Object.defineProperty(exports, 'JSXElement', {
  enumerable: true,
  get: function get() {
    return _interopRequireDefault(_JSXElement).default;
  }
});

var _JSXAttribute = require('./JSXAttribute');

Object.defineProperty(exports, 'JSXAttribute', {
  enumerable: true,
  get: function get() {
    return _interopRequireDefault(_JSXAttribute).default;
  }
});

var _JSXComment = require('./JSXComment');

Object.defineProperty(exports, 'JSXComment', {
  enumerable: true,
  get: function get() {
    return _interopRequireDefault(_JSXComment).default;
  }
});

var _JSXText = require('./JSXText');

Object.defineProperty(exports, 'JSXText', {
  enumerable: true,
  get: function get() {
    return _interopRequireDefault(_JSXText).default;
  }
});
exports.default = transformJsx;

var _visitor = require('./visitor');

var _visitor2 = _interopRequireDefault(_visitor);

var _generator = require('./generator');

var _generator2 = _interopRequireDefault(_generator);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function transformJsx() {
  return { visitor: _visitor2.default, generator: _generator2.default };
}