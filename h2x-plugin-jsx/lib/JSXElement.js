'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _h2xTypes = require('h2x-types');

class JSXElement {
  constructor() {
    this.name = null;
    this.children = [];
    this.attributes = [];
  }

}

JSXElement[_h2xTypes.NODE_TYPE] = 'JSXElement';
JSXElement[_h2xTypes.VISITOR_KEYS] = ['children', 'attributes'];
exports.default = JSXElement;