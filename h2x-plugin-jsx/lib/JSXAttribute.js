'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _h2xTypes = require('h2x-types');

class JSXAttribute {
  constructor() {
    this.name = null;
    this.value = null;
    this.literal = false;
    this.spread = false;
  }

}

JSXAttribute[_h2xTypes.NODE_TYPE] = 'JSXAttribute';
JSXAttribute[_h2xTypes.VISITOR_KEYS] = null;
exports.default = JSXAttribute;