3.0.0 / 2018-05-31
==================
  * [Breaking] update to match latest spec
  * [Deps] update `es-abstract`
  * [Dev Deps] update `eslint`, `nsp`, `object-inspect`, `tape`
  * [Tests] up to `node` `v10.3`, `v9.11`, `v8.11`, `v6.14`, `v4.9`
  * [Tests] regexes now have a "groups" property in ES2018
  * [Tests] run evalmd in prelint

2.0.0 / 2018-01-24
==================
  * [Breaking] change to handle nonmatching regexes
  * [Breaking] non-regex arguments that are thus coerced to RegExp now get the global flag
  * [Deps] update `es-abstract`, `regexp.prototype.flags`
  * [Dev Deps] update `es5-shim`, `eslint`, `object.assign`
  * [Tests] up to `node` `v9.4`, `v8.9`, `v6.12`; pin included builds to LTS
  * [Tests] improve and correct tests and failure messages

1.0.0 / 2017-09-28
==================
  * Initial release
